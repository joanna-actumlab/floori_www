<?php
/**
 * Template part for displaying contact
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package floori
 */

?>

<section id="contact" class="contact">
    <div class="contact-wrapper">
        <?php if( have_rows('find-us',6) ): ?>
        <div class="contact-info">
            <?php while ( have_rows('find-us',6) ) : the_row(); ?>
            <h2 data-aos="fade-up">
                <?php the_sub_field('find-us-title');?>
            </h2>
            <?php if( have_rows('numbers',6) ): ?>
            <div class="contact-numbers">
                <?php while ( have_rows('numbers',6) ) : the_row(); ?>
                <p style="margin-bottom:0px;"><span data-aos="fade-up">
                        <?php the_sub_field('city-item');?></span></p>
                <p style="margin-bottom:0px; text-align: right;"><a href="tel:+1-616-485-6925" data-aos="fade-up">
                        <?php the_sub_field('number-item');?></a></p>
                <?php endwhile;?>
            </div>
            <?php endif;?>
            <div class="contact-numbers"></div>
            <?php echo do_shortcode('[wpgmza id="1"]');?>
            <?php endwhile;?>
        </div>
        <?php endif;?>
        <?php if( have_rows('contact-us',6) ): ?>
        <div class="contact-form">

            <?php while ( have_rows('contact-us',6) ) : the_row(); ?>
            <h2 data-aos="fade-up">
                <?php the_sub_field('contact-title');?>
            </h2>
            <p data-aos="fade-up">Questions, ideas or feedback?<br> Send us a message and we’ll get back to you as soon as possible!</p>
            <?php echo do_shortcode('[contact-form-7 id="145" title="Form-eng"]');?>
            <?php endwhile;?>

        </div>
        <?php endif;?>
    </div>
</section>
<!-- #post-<?php the_ID(); ?> -->
