/**
 * alertifyjs 1.11.1 http://alertifyjs.com
 * AlertifyJS is a javascript framework for developing pretty browser dialogs and notifications.
 * Copyright 2018 Mohammad Younes <Mohammad@alertifyjs.com> (http://alertifyjs.com) 
 * Licensed under GPL 3 <https://opensource.org/licenses/gpl-3.0>*/
!function(Re){"use strict";
/**
     * Keys enum
     * @type {Object}
     */
/**
     * [Helper]  Adds the specified class(es) to the element.
     *
     * @element {node}      The element
     * @className {string}  One or more space-separated classes to be added to the class attribute of the element.
     * 
     * @return {undefined}
     */
function De(e,t){e.className+=" "+t}
/**
     * [Helper]  Removes the specified class(es) from the element.
     *
     * @element {node}      The element
     * @className {string}  One or more space-separated classes to be removed from the class attribute of the element.
     * 
     * @return {undefined}
     */function Fe(e,t){for(var n=e.className.split(" "),i=t.split(" "),s=0;s<i.length;s+=1){var o=n.indexOf(i[s]);-1<o&&n.splice(o,1)}e.className=n.join(" ")}
/**
     * [Helper]  Checks if the document is RTL
     *
     * @return {Boolean} True if the document is RTL, false otherwise.
     */function Ue(){return"rtl"===Re.getComputedStyle(document.body).direction}
/**
     * [Helper]  Get the document current scrollTop
     *
     * @return {Number} current document scrollTop value
     */function Xe(){return document.documentElement&&document.documentElement.scrollTop||document.body.scrollTop}
/**
     * [Helper]  Get the document current scrollLeft
     *
     * @return {Number} current document scrollLeft value
     */function Ye(){return document.documentElement&&document.documentElement.scrollLeft||document.body.scrollLeft}
/**
    * Helper: clear contents
    *
    */function qe(e){for(;e.lastChild;)e.removeChild(e.lastChild)}
/**
     * Extends a given prototype by merging properties from base into sub.
     *
     * @sub {Object} sub The prototype being overwritten.
     * @base {Object} base The prototype being written.
     *
     * @return {Object} The extended prototype.
     */function Je(e){if(null===e)return e;var t;if(Array.isArray(e)){t=[];for(var n=0;n<e.length;n+=1)t.push(Je(e[n]));return t}if(e instanceof Date)return new Date(e.getTime());if(e instanceof RegExp)return(t=new RegExp(e.source)).global=e.global,t.ignoreCase=e.ignoreCase,t.multiline=e.multiline,t.lastIndex=e.lastIndex,t;if("object"!=typeof e)return e;
// copy dialog pototype over definition.
for(var i in t={},e)e.hasOwnProperty(i)&&(t[i]=Je(e[i]));return t}
/**
      * Helper: destruct the dialog
      *
      */function Ke(e,t){
//delete the dom and it's references.
var n=e.elements.root;n.parentNode.removeChild(n),delete e.elements,
//copy back initial settings.
e.settings=Je(e.__settings),
//re-reference init function.
e.__init=t,
//delete __internal variable to allow re-initialization.
delete e.__internal}
/**
     * Use a closure to return proper event listener method. Try to use
     * `addEventListener` by default but fallback to `attachEvent` for
     * unsupported browser. The closure simply ensures that the test doesn't
     * happen every time the method is called.
     *
     * @param    {Node}     el    Node element
     * @param    {String}   event Event type
     * @param    {Function} fn    Callback of event
     * @return   {Function}
     */
/**
    * Creates event handler delegate that sends the instance as last argument.
    * 
    * @return {Function}    a function wrapper which sends the instance as last argument.
    */
function Ve(n,i){return function(){if(0<arguments.length){for(var e=[],t=0;t<arguments.length;t+=1)e.push(arguments[t]);return e.push(n),i.apply(n,e)}return i.apply(n,[null,n])}}
/**
    * Helper for creating a dialog close event.
    * 
    * @return {object}
    */function Ge(e,t){return{index:e,button:t,cancel:!1}}
/**
    * Helper for dispatching events.
    *
    * @param  {string} evenType The type of the event to disptach.
    * @param  {object} instance The dialog instance disptaching the event.
    *
    * @return   {any}   The result of the invoked function.
    */function Qe(e,t){if("function"==typeof t.get(e))return t.get(e).call(t)}
/**
     * Super class for all dialogs
     *
     * @return {Object}		base dialog prototype
     */
/**
     * Alertify public API
     * This contains everything that is exposed through the alertify object.
     *
     * @return {Object}
     */
function e(){
/**
         * Extends a given prototype by merging properties from base into sub.
         *
         * @sub {Object} sub The prototype being overwritten.
         * @base {Object} base The prototype being written.
         *
         * @return {Object} The extended prototype.
         */
function o(e,t){
// copy dialog pototype over definition.
for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n]);return e}
/**
        * Helper: returns a dialog instance from saved dialogs.
        * and initializes the dialog if its not already initialized.
        *
        * @name {String} name The dialog name.
        *
        * @return {Object} The dialog instance.
        */function a(e){var t=r[e].dialog;
//initialize the dialog if its not already initialized.
return t&&"function"==typeof t.__init&&t.__init(t),t}
/**
         * Helper:  registers a new dialog definition.
         *
         * @name {String} name The dialog name.
         * @Factory {Function} Factory a function resposible for creating dialog prototype.
         * @transient {Boolean} transient True to create a new dialog instance each time the dialog is invoked, false otherwise.
         * @base {String} base the name of another dialog to inherit from.
         *
         * @return {Object} The dialog definition.
         */function l(e,t,n,i){var s={dialog:null,factory:t};
//if this is based on an existing dialog, create a new definition
//by applying the new protoype over the existing one.
return void 0!==i&&(s.factory=function(){return o(new r[i].factory,new t)}),n||(
//create a new definition based on dialog
s.dialog=o(new s.factory,c)),r[e]=s}
// holds a references of created dialogs
var r={};return{
/**
             * Alertify defaults
             * 
             * @type {Object}
             */
defaults:t,
/**
             * Dialogs factory 
             *
             * @param {string}      Dialog name.
             * @param {Function}    A Dialog factory function.
             * @param {Boolean}     Indicates whether to create a singleton or transient dialog.
             * @param {String}      The name of the base type to inherit from.
             */
dialog:function(e,t,n,i){
// get request, create a new instance and return it.
if("function"!=typeof t)return a(e);if(this.hasOwnProperty(e))throw new Error("alertify.dialog: name already exists");
// register the dialog
var s=l(e,t,n,i);
// make it public
this[e]=n?function(){
//if passed with no params, consider it a get request
if(0===arguments.length)return s.dialog;var e=o(new s.factory,c);
//ensure init
return e&&"function"==typeof e.__init&&e.__init(e),e.main.apply(e,arguments),e.show.apply(e)}:function(){
//if passed with no params, consider it a get request
if(
//ensure init
s.dialog&&"function"==typeof s.dialog.__init&&s.dialog.__init(s.dialog),0===arguments.length)return s.dialog;var e=s.dialog;return e.main.apply(s.dialog,arguments),e.show.apply(s.dialog)}},
/**
             * Close all open dialogs.
             *
             * @param {Object} excpet [optional] The dialog object to exclude from closing.
             *
             * @return {undefined}
             */
closeAll:function(e){for(var t=it.slice(0),n=0;n<t.length;n+=1){var i=t[n];void 0!==e&&e===i||i.close()}},
/**
             * Gets or Sets dialog settings/options. if the dialog is transient, this call does nothing.
             *
             * @param {string} name The dialog name.
             * @param {String|Object} key A string specifying a propery name or a collection of key/value pairs.
             * @param {Variant} value Optional, the value associated with the key (in case it was a string).
             *
             * @return {undefined}
             */
setting:function(e,t,n){if("notifier"===e)return u.setting(t,n);var i=a(e);return i?i.setting(t,n):void 0},
/**
             * [Alias] Sets dialog settings/options 
             */
set:function(e,t,n){return this.setting(e,t,n)},
/**
             * [Alias] Gets dialog settings/options 
             */
get:function(e,t){return this.setting(e,t)},
/**
             * Creates a new notification message.
             * If a type is passed, a class name "ajs-{type}" will be added.
             * This allows for custom look and feel for various types of notifications.
             *
             * @param  {String | DOMElement}    [message=undefined]		Message text
             * @param  {String}                 [type='']				Type of log message
             * @param  {String}                 [wait='']				Time (in seconds) to wait before auto-close
             * @param  {Function}               [callback=undefined]	A callback function to be invoked when the log is closed.
             *
             * @return {Object} Notification object.
             */
notify:function(e,t,n,i){return u.create(t,i).push(e,n)},
/**
             * Creates a new notification message.
             *
             * @param  {String}		[message=undefined]		Message text
             * @param  {String}     [wait='']				Time (in seconds) to wait before auto-close
             * @param  {Function}	[callback=undefined]	A callback function to be invoked when the log is closed.
             *
             * @return {Object} Notification object.
             */
message:function(e,t,n){return u.create(null,n).push(e,t)},
/**
             * Creates a new notification message of type 'success'.
             *
             * @param  {String}		[message=undefined]		Message text
             * @param  {String}     [wait='']				Time (in seconds) to wait before auto-close
             * @param  {Function}	[callback=undefined]	A callback function to be invoked when the log is closed.
             *
             * @return {Object} Notification object.
             */
success:function(e,t,n){return u.create("success",n).push(e,t)},
/**
             * Creates a new notification message of type 'error'.
             *
             * @param  {String}		[message=undefined]		Message text
             * @param  {String}     [wait='']				Time (in seconds) to wait before auto-close
             * @param  {Function}	[callback=undefined]	A callback function to be invoked when the log is closed.
             *
             * @return {Object} Notification object.
             */
error:function(e,t,n){return u.create("error",n).push(e,t)},
/**
             * Creates a new notification message of type 'warning'.
             *
             * @param  {String}		[message=undefined]		Message text
             * @param  {String}     [wait='']				Time (in seconds) to wait before auto-close
             * @param  {Function}	[callback=undefined]	A callback function to be invoked when the log is closed.
             *
             * @return {Object} Notification object.
             */
warning:function(e,t,n){return u.create("warning",n).push(e,t)},
/**
             * Dismisses all open notifications
             *
             * @return {undefined}
             */
dismissAll:function(){u.dismissAll()}}}var n=13,Ze=27,$e=112,et=123,tt=37,nt=39,t={autoReset:!0,basic:!1,closable:!0,closableByDimmer:!0,frameless:!1,maintainFocus:!0,//global default not per instance, applies to all dialogs
maximizable:!0,modal:!0,movable:!0,moveBounded:!1,overflow:!0,padding:!0,pinnable:!0,pinned:!0,preventBodyShift:!1,//global default not per instance, applies to all dialogs
resizable:!0,startMaximized:!1,transition:"pulse",notifier:{delay:5,position:"bottom-right",closeButton:!1},glossary:{title:"AlertifyJS",ok:"OK",cancel:"Cancel",acccpt:"Accept",deny:"Deny",confirm:"Confirm",decline:"Decline",close:"Close",maximize:"Maximize",restore:"Restore"},theme:{input:"ajs-input",ok:"ajs-ok",cancel:"ajs-cancel"}},it=[],st=document.addEventListener?function(e,t,n,i){e.addEventListener(t,n,!0===i)}:document.attachEvent?function(e,t,n){e.attachEvent("on"+t,n)}:void 0,ot=document.removeEventListener?function(e,t,n,i){e.removeEventListener(t,n,!0===i)}:document.detachEvent?function(e,t,n){e.detachEvent("on"+t,n)}:void 0,at=function(){var e,t,n=!1,i={animation:"animationend",OAnimation:"oAnimationEnd oanimationend",msAnimation:"MSAnimationEnd",MozAnimation:"animationend",WebkitAnimation:"webkitAnimationEnd"};for(e in i)if(void 0!==document.documentElement.style[e]){t=i[e],n=!0;break}return{type:t,supported:n}}(),c=function(){
/**
         * Helper: initializes the dialog instance
         * 
         * @return	{Number}	The total count of currently open modals.
         */
function s(e){if(!e.__internal){
//get dialog buttons/focus setup
var t;
//no need to expose init after this.
delete e.__init,
//keep a copy of initial dialog settings
e.__settings||(e.__settings=Je(e.settings)),"function"==typeof e.setup?((t=e.setup()).options=t.options||{},t.focus=t.focus||{}):t={buttons:[],focus:{element:null,select:!1},options:{}},
//initialize hooks object.
"object"!=typeof e.hooks&&(e.hooks={});
//copy buttons defintion
var n=[];if(Array.isArray(t.buttons))for(var i=0;i<t.buttons.length;i+=1){var s=t.buttons[i],o={};for(var a in s)s.hasOwnProperty(a)&&(o[a]=s[a]);n.push(o)}var l=e.__internal={
/**
                     * Flag holding the open state of the dialog
                     * 
                     * @type {Boolean}
                     */
isOpen:!1,
/**
                     * Active element is the element that will receive focus after
                     * closing the dialog. It defaults as the body tag, but gets updated
                     * to the last focused element before the dialog was opened.
                     *
                     * @type {Node}
                     */
activeElement:document.body,timerIn:void 0,timerOut:void 0,buttons:n,focus:t.focus,options:{title:void 0,modal:void 0,basic:void 0,frameless:void 0,pinned:void 0,movable:void 0,moveBounded:void 0,resizable:void 0,autoReset:void 0,closable:void 0,closableByDimmer:void 0,maximizable:void 0,startMaximized:void 0,pinnable:void 0,transition:void 0,padding:void 0,overflow:void 0,onshow:void 0,onclosing:void 0,onclose:void 0,onfocus:void 0,onmove:void 0,onmoved:void 0,onresize:void 0,onresized:void 0,onmaximize:void 0,onmaximized:void 0,onrestore:void 0,onrestored:void 0},resetHandler:void 0,beginMoveHandler:void 0,beginResizeHandler:void 0,bringToFrontHandler:void 0,modalClickHandler:void 0,buttonsClickHandler:void 0,commandsClickHandler:void 0,transitionInHandler:void 0,transitionOutHandler:void 0,destroy:void 0},r={};
//root node
r.root=document.createElement("div"),r.root.className=_e.base+" "+_e.hidden+" ",r.root.innerHTML=re+ce,
//dimmer
r.dimmer=r.root.firstChild,
//dialog
r.modal=r.root.lastChild,r.modal.innerHTML=de,r.dialog=r.modal.firstChild,r.dialog.innerHTML=ue+me+fe+he+be+ye+ue,
//reset links
r.reset=[],r.reset.push(r.dialog.firstChild),r.reset.push(r.dialog.lastChild),
//commands
r.commands={},r.commands.container=r.reset[0].nextSibling,r.commands.pin=r.commands.container.firstChild,r.commands.maximize=r.commands.pin.nextSibling,r.commands.close=r.commands.maximize.nextSibling,
//header
r.header=r.commands.container.nextSibling,
//body
r.body=r.header.nextSibling,r.body.innerHTML=pe,r.content=r.body.firstChild,
//footer
r.footer=r.body.nextSibling,r.footer.innerHTML=ve.auxiliary+ve.primary,
//resize handle
r.resizeHandle=r.footer.nextSibling,
//buttons
r.buttons={},r.buttons.auxiliary=r.footer.firstChild,r.buttons.primary=r.buttons.auxiliary.nextSibling,r.buttons.primary.innerHTML=ge,r.buttonTemplate=r.buttons.primary.firstChild,
//remove button template
r.buttons.primary.removeChild(r.buttonTemplate);for(var c=0;c<e.__internal.buttons.length;c+=1){var d=e.__internal.buttons[c];
// add to the list of used keys.
for(var u in se.indexOf(d.key)<0&&se.push(d.key),d.element=r.buttonTemplate.cloneNode(),d.element.innerHTML=d.text,"string"==typeof d.className&&""!==d.className&&De(d.element,d.className),d.attrs)"className"!==u&&d.attrs.hasOwnProperty(u)&&d.element.setAttribute(u,d.attrs[u]);"auxiliary"===d.scope?r.buttons.auxiliary.appendChild(d.element):r.buttons.primary.appendChild(d.element)}
//make elements pubic
//settings
for(var m in e.elements=r,
//save event handlers delegates
l.resetHandler=Ve(e,N),l.beginMoveHandler=Ve(e,W),l.beginResizeHandler=Ve(e,U),l.bringToFrontHandler=Ve(e,f),l.modalClickHandler=Ve(e,w),l.buttonsClickHandler=Ve(e,T),l.commandsClickHandler=Ve(e,p),l.transitionInHandler=Ve(e,L),l.transitionOutHandler=Ve(e,A),l.options)void 0!==t.options[m]?
// if found in user options
e.set(m,t.options[m]):lt.defaults.hasOwnProperty(m)?
// else if found in defaults options
e.set(m,lt.defaults[m]):"title"===m&&
// else if title key, use alertify.defaults.glossary
e.set(m,lt.defaults.glossary[m]);
// allow dom customization
"function"==typeof e.build&&e.build()}
//add to the end of the DOM tree.
document.body.appendChild(e.elements.root)}
/**
         * Helper: maintains scroll position
         *
         */function o(){ke=Ye(),xe=Xe()}function n(){Re.scrollTo(ke,xe)}
/**
         * Helper: adds/removes no-overflow class from body
         *
         */function a(){for(var e=0,t=0;t<it.length;t+=1){var n=it[t];(n.isModal()||n.isMaximized())&&(e+=1)}0===e&&0<=document.body.className.indexOf(_e.noOverflow)?(
//last open modal or last maximized one
Fe(document.body,_e.noOverflow),i(!1)):0<e&&document.body.className.indexOf(_e.noOverflow)<0&&(
//first open modal or first maximized one
i(!0),De(document.body,_e.noOverflow))}
/**
         * Helper: prevents body shift.
         *
         */
function i(e){lt.defaults.preventBodyShift&&document.documentElement.scrollHeight>document.documentElement.clientHeight&&(e?(//&& openDialogs[openDialogs.length-1].elements.dialog.clientHeight <= document.documentElement.clientHeight){
ze=xe,He=Re.getComputedStyle(document.body).top,De(document.body,_e.fixed),document.body.style.top=-xe+"px"):(xe=ze,document.body.style.top=He,Fe(document.body,_e.fixed),n()))}
/**
         * Sets the name of the transition used to show/hide the dialog
         * 
         * @param {Object} instance The dilog instance.
         *
         */function l(e,t,n){"string"==typeof n&&Fe(e.elements.root,_e.prefix+n),De(e.elements.root,_e.prefix+t),oe=e.elements.root.offsetWidth}
/**
         * Toggles the dialog display mode
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function r(e){e.get("modal")?(
//make modal
Fe(e.elements.root,_e.modeless),
//only if open
e.isOpen()&&(Q(e),
//in case a pinned modless dialog was made modal while open.
x(e),a())):(
//make modelss
De(e.elements.root,_e.modeless),
//only if open
e.isOpen()&&(G(e),
//in case pin/unpin was called while a modal is open
x(e),a()))}
/**
         * Toggles the dialog basic view mode 
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function c(e){e.get("basic")?
// add class
De(e.elements.root,_e.basic):
// remove class
Fe(e.elements.root,_e.basic)}
/**
         * Toggles the dialog frameless view mode 
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function d(e){e.get("frameless")?
// add class
De(e.elements.root,_e.frameless):
// remove class
Fe(e.elements.root,_e.frameless)}
/**
         * Helper: Brings the modeless dialog to front, attached to modeless dialogs.
         *
         * @param {Event} event Focus event
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function f(e,t){for(
// Do not bring to front if preceeded by an open modal
var n,i=it.indexOf(t)+1;i<it.length;i+=1)if(it[i].isModal())return;
// Bring to front by making it the last child.
return document.body.lastChild!==t.elements.root&&(document.body.appendChild(t.elements.root),
//also make sure its at the end of the list
it.splice(it.indexOf(t),1),it.push(t),E(t)),!1}
/**
         * Helper: reflects dialogs options updates
         *
         * @param {Object} instance The dilog instance.
         * @param {String} option The updated option name.
         *
         * @return	{undefined}	
         */function u(e,t,n,i){switch(t){case"title":e.setHeader(i);break;case"modal":r(e);break;case"basic":c(e);break;case"frameless":d(e);break;case"pinned":H(e);break;case"closable":O(e);break;case"maximizable":z(e);break;case"pinnable":_(e);break;case"movable":D(e);break;case"resizable":J(e);break;case"padding":i?Fe(e.elements.root,_e.noPadding):e.elements.root.className.indexOf(_e.noPadding)<0&&De(e.elements.root,_e.noPadding);break;case"overflow":i?Fe(e.elements.root,_e.noOverflow):e.elements.root.className.indexOf(_e.noOverflow)<0&&De(e.elements.root,_e.noOverflow);break;case"transition":l(e,i,n);break}
// internal on option updated event
"function"==typeof e.hooks.onupdate&&e.hooks.onupdate.call(e,t,n,i)}
/**
         * Helper: reflects dialogs options updates
         *
         * @param {Object} instance The dilog instance.
         * @param {Object} obj The object to set/get a value on/from.
         * @param {Function} callback The callback function to call if the key was found.
         * @param {String|Object} key A string specifying a propery name or a collection of key value pairs.
         * @param {Object} value Optional, the value associated with the key (in case it was a string).
         * @param {String} option The updated option name.
         *
         * @return	{Object} result object 
         *	The result objects has an 'op' property, indicating of this is a SET or GET operation.
         *		GET: 
         *		- found: a flag indicating if the key was found or not.
         *		- value: the property value.
         *		SET:
         *		- items: a list of key value pairs of the properties being set.
         *				each contains:
         *					- found: a flag indicating if the key was found or not.
         *					- key: the property key.
         *					- value: the property value.
         */function m(e,t,n,i,s){var o={op:void 0,items:[]},a;if(void 0===s&&"string"==typeof i)
//get
o.op="get",t.hasOwnProperty(i)?(o.found=!0,o.value=t[i]):(o.found=!1,o.value=void 0);else if(
//set
o.op="set","object"==typeof i){
//set multiple
var l=i;for(var r in l)t.hasOwnProperty(r)?(t[r]!==l[r]&&(a=t[r],t[r]=l[r],n.call(e,r,a,l[r])),o.items.push({key:r,value:l[r],found:!0})):o.items.push({key:r,value:l[r],found:!1})}else{if("string"!=typeof i)
//invalid params
throw new Error("args must be a string or object");
//set single
t.hasOwnProperty(i)?(t[i]!==s&&(a=t[i],t[i]=s,n.call(e,i,a,s)),o.items.push({key:i,value:s,found:!0})):o.items.push({key:i,value:s,found:!1})}return o}
/**
         * Triggers a close event.
         *
         * @param {Object} instance	The dilog instance.
         * 
         * @return {undefined}
         */function h(e){var t;C(e,function(e){return t=!0===e.invokeOnClose}),
//none of the buttons registered as onclose callback
//close the dialog
!t&&e.isOpen()&&e.close()}
/**
         * Dialogs commands event handler, attached to the dialog commands element.
         *
         * @param {Event} event	DOM event object.
         * @param {Object} instance	The dilog instance.
         * 
         * @return {undefined}
         */function p(e,t){var n;switch(e.srcElement||e.target){case t.elements.commands.pin:t.isPinned()?v(t):b(t);break;case t.elements.commands.maximize:t.isMaximized()?y(t):g(t);break;case t.elements.commands.close:h(t);break}return!1}
/**
         * Helper: pins the modeless dialog.
         *
         * @param {Object} instance	The dialog instance.
         * 
         * @return {undefined}
         */function b(e){
//pin the dialog
e.set("pinned",!0)}
/**
         * Helper: unpins the modeless dialog.
         *
         * @param {Object} instance	The dilog instance.
         * 
         * @return {undefined}
         */function v(e){
//unpin the dialog 
e.set("pinned",!1)}
/**
         * Helper: enlarges the dialog to fill the entire screen.
         *
         * @param {Object} instance	The dilog instance.
         * 
         * @return {undefined}
         */function g(e){
// allow custom `onmaximize` method
Qe("onmaximize",e),
//maximize the dialog 
De(e.elements.root,_e.maximized),e.isOpen()&&a(),
// allow custom `onmaximized` method
Qe("onmaximized",e)}
/**
         * Helper: returns the dialog to its former size.
         *
         * @param {Object} instance	The dilog instance.
         * 
         * @return {undefined}
         */function y(e){
// allow custom `onrestore` method
Qe("onrestore",e),
//maximize the dialog 
Fe(e.elements.root,_e.maximized),e.isOpen()&&a(),
// allow custom `onrestored` method
Qe("onrestored",e)}
/**
         * Show or hide the maximize box.
         *
         * @param {Object} instance The dilog instance.
         * @param {Boolean} on True to add the behavior, removes it otherwise.
         *
         * @return {undefined}
         */function _(e){e.get("pinnable")?
// add class
De(e.elements.root,_e.pinnable):
// remove class
Fe(e.elements.root,_e.pinnable)}
/**
         * Helper: Fixes the absolutly positioned modal div position.
         *
         * @param {Object} instance The dialog instance.
         *
         * @return {undefined}
         */function t(e){var t=Ye();e.elements.modal.style.marginTop=Xe()+"px",e.elements.modal.style.marginLeft=t+"px",e.elements.modal.style.marginRight=-t+"px"}
/**
         * Helper: Removes the absolutly positioned modal div position fix.
         *
         * @param {Object} instance The dialog instance.
         *
         * @return {undefined}
         */function k(e){var t=parseInt(e.elements.modal.style.marginTop,10),n=parseInt(e.elements.modal.style.marginLeft,10);if(e.elements.modal.style.marginTop="",e.elements.modal.style.marginLeft="",e.elements.modal.style.marginRight="",e.isOpen()){var i=0,s=0;""!==e.elements.dialog.style.top&&(i=parseInt(e.elements.dialog.style.top,10)),e.elements.dialog.style.top=i+(t-Xe())+"px",""!==e.elements.dialog.style.left&&(s=parseInt(e.elements.dialog.style.left,10)),e.elements.dialog.style.left=s+(n-Ye())+"px"}}
/**
         * Helper: Adds/Removes the absolutly positioned modal div position fix based on its pinned setting.
         *
         * @param {Object} instance The dialog instance.
         *
         * @return {undefined}
         */function x(e){
// if modeless and unpinned add fix
e.get("modal")||e.get("pinned")?k(e):t(e)}
/**
         * Toggles the dialog position lock | modeless only.
         *
         * @param {Object} instance The dilog instance.
         * @param {Boolean} on True to make it modal, false otherwise.
         *
         * @return {undefined}
         */function H(e){e.get("pinned")?(Fe(e.elements.root,_e.unpinned),e.isOpen()&&k(e)):(De(e.elements.root,_e.unpinned),e.isOpen()&&!e.isModal()&&t(e))}
/**
         * Show or hide the maximize box.
         *
         * @param {Object} instance The dilog instance.
         * @param {Boolean} on True to add the behavior, removes it otherwise.
         *
         * @return {undefined}
         */function z(e){e.get("maximizable")?
// add class
De(e.elements.root,_e.maximizable):
// remove class
Fe(e.elements.root,_e.maximizable)}
/**
         * Show or hide the close box.
         *
         * @param {Object} instance The dilog instance.
         * @param {Boolean} on True to add the behavior, removes it otherwise.
         *
         * @return {undefined}
         */function O(e){e.get("closable")?(
// add class
De(e.elements.root,_e.closable),ne(e)):(
// remove class
Fe(e.elements.root,_e.closable),ie(e))}
// flag to cancel click event if already handled by end resize event (the mousedown, mousemove, mouseup sequence fires a click event.).
/**
         * Helper: closes the modal dialog when clicking the modal
         *
         * @param {Event} event	DOM event object.
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */
function w(e,t){var n=e.srcElement||e.target;return Oe||n!==t.elements.modal||!0!==t.get("closableByDimmer")||h(t),Oe=!1}
// flag to cancel keyup event if already handled by click event (pressing Enter on a focusted button).
/** 
         * Helper: triggers a button callback
         *
         * @param {Object}		The dilog instance.
         * @param {Function}	Callback to check which button triggered the event.
         *
         * @return {undefined}
         */
function C(e,t){for(var n=0;n<e.__internal.buttons.length;n+=1){var i=e.__internal.buttons[n];if(!i.element.disabled&&t(i)){var s=Ge(n,i);"function"==typeof e.callback&&e.callback.apply(e,[s]),
//close the dialog only if not canceled.
!1===s.cancel&&e.close();break}}}
/**
         * Clicks event handler, attached to the dialog footer.
         *
         * @param {Event}		DOM event object.
         * @param {Object}		The dilog instance.
         * 
         * @return {undefined}
         */function T(e,t){var n=e.srcElement||e.target;C(t,function(e){
// if this button caused the click, cancel keyup event
return e.element===n&&(we=!0)})}
/**
         * Keyup event handler, attached to the document.body
         *
         * @param {Event}		DOM event object.
         * @param {Object}		The dilog instance.
         * 
         * @return {undefined}
         */function M(e){
//hitting enter while button has focus will trigger keyup too.
//ignore if handled by clickHandler
if(!we){var t=it[it.length-1],n=e.keyCode;return 0===t.__internal.buttons.length&&n===Ze&&!0===t.get("closable")?(h(t),!1):-1<se.indexOf(n)?(C(t,function(e){return e.key===n}),!1):void 0}we=!1}
/**
        * Keydown event handler, attached to the document.body
        *
        * @param {Event}		DOM event object.
        * @param {Object}		The dilog instance.
        * 
        * @return {undefined}
        */function j(e){var t=it[it.length-1],n=e.keyCode;if(n===tt||n===nt){for(var i=t.__internal.buttons,s=0;s<i.length;s+=1)if(document.activeElement===i[s].element)switch(n){case tt:return void i[(s||i.length)-1].element.focus();case nt:return void i[(s+1)%i.length].element.focus()}}else if(n<et+1&&$e-1<n&&-1<se.indexOf(n))return e.preventDefault(),e.stopPropagation(),C(t,function(e){return e.key===n}),!1}
/**
         * Sets focus to proper dialog element
         *
         * @param {Object} instance The dilog instance.
         * @param {Node} [resetTarget=undefined] DOM element to reset focus to.
         *
         * @return {undefined}
         */function E(e,t){
// reset target has already been determined.
if(t)t.focus();else{
// current instance focus settings
var n=e.__internal.focus,i=n.element;
// the focus element.
switch(typeof n.element){
// a number means a button index
case"number":e.__internal.buttons.length>n.element&&(
//in basic view, skip focusing the buttons.
i=!0===e.get("basic")?e.elements.reset[0]:e.__internal.buttons[n.element].element);break;
// a string means querySelector to select from dialog body contents.
case"string":i=e.elements.body.querySelector(n.element);break;
// a function should return the focus element.
case"function":i=n.element.call(e);break}
// if no focus element, default to first reset element.
null==i&&0===e.__internal.buttons.length&&(i=e.elements.reset[0]),
// focus
i&&i.focus&&(i.focus(),
// if selectable
n.select&&i.select&&i.select())}}
/**
         * Focus event handler, attached to document.body and dialogs own reset links.
         * handles the focus for modal dialogs only.
         *
         * @param {Event} event DOM focus event object.
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function N(e,t){
// should work on last modal if triggered from document.body 
if(!t)for(var n=it.length-1;-1<n;n-=1)if(it[n].isModal()){t=it[n];break}
// if modal
if(t&&t.isModal()){
// determine reset target to enable forward/backward tab cycle.
var i,s=e.srcElement||e.target,o=s===t.elements.reset[1]||0===t.__internal.buttons.length&&s===document.body;
// if last reset link, then go to maximize or close
o&&(t.get("maximizable")?i=t.elements.commands.maximize:t.get("closable")&&(i=t.elements.commands.close)),
// if no reset target found, try finding the best button
void 0===i&&("number"==typeof t.__internal.focus.element?
// button focus element, go to first available button
s===t.elements.reset[0]?i=t.elements.buttons.auxiliary.firstChild||t.elements.buttons.primary.firstChild:o&&(
//restart the cycle by going to first reset link
i=t.elements.reset[0]):
// will reach here when tapping backwards, so go to last child
// The focus element SHOULD NOT be a button (logically!).
s===t.elements.reset[0]&&(i=t.elements.buttons.primary.lastChild||t.elements.buttons.auxiliary.lastChild)),
// focus
E(t,i)}}
/**
         * Transition in transitionend event handler. 
         *
         * @param {Event}		TransitionEnd event object.
         * @param {Object}		The dilog instance.
         *
         * @return {undefined}
         */function L(e,t){
// clear the timer
clearTimeout(t.__internal.timerIn),
// once transition is complete, set focus
E(t),
//restore scroll to prevent document jump
n(),
// allow handling key up after transition ended.
we=!1,
// allow custom `onfocus` method
Qe("onfocus",t),
// unbind the event
ot(t.elements.dialog,at.type,t.__internal.transitionInHandler),Fe(t.elements.root,_e.animationIn)}
/**
         * Transition out transitionend event handler. 
         *
         * @param {Event}		TransitionEnd event object.
         * @param {Object}		The dilog instance.
         *
         * @return {undefined}
         */function A(e,t){
// clear the timer
clearTimeout(t.__internal.timerOut),
// unbind the event
ot(t.elements.dialog,at.type,t.__internal.transitionOutHandler),
// reset move updates
R(t),
// reset resize updates
q(t),
// restore if maximized
t.isMaximized()&&!t.get("startMaximized")&&y(t),
// return focus to the last active element
lt.defaults.maintainFocus&&t.__internal.activeElement&&(t.__internal.activeElement.focus(),t.__internal.activeElement=null),
//destory the instance
"function"==typeof t.__internal.destroy&&t.__internal.destroy.apply(t)}
/* Controls moving a dialog around */
//holde the current moving instance
/**
         * Helper: sets the element top/left coordinates
         *
         * @param {Event} event	DOM event object.
         * @param {Node} element The element being moved.
         * 
         * @return {undefined}
         */
function I(e,t){var n=e[je]-Te,i=e[Ee]-Me;Le&&(i-=document.body.scrollTop),t.style.left=n+"px",t.style.top=i+"px"}
/**
         * Helper: sets the element top/left coordinates within screen bounds
         *
         * @param {Event} event	DOM event object.
         * @param {Node} element The element being moved.
         * 
         * @return {undefined}
         */function P(e,t){var n=e[je]-Te,i=e[Ee]-Me;Le&&(i-=document.body.scrollTop),t.style.left=Math.min(Ne.maxLeft,Math.max(Ne.minLeft,n))+"px",t.style.top=Le?Math.min(Ne.maxTop,Math.max(Ne.minTop,i))+"px":Math.max(Ne.minTop,i)+"px"}
/**
         * Triggers the start of a move event, attached to the header element mouse down event.
         * Adds no-selection class to the body, disabling selection while moving.
         *
         * @param {Event} event	DOM event object.
         * @param {Object} instance The dilog instance.
         * 
         * @return {Boolean} false
         */function W(e,t){if(null===Ie&&!t.isMaximized()&&t.get("movable")){var n,i=0,s=0;if("touchstart"===e.type?(e.preventDefault(),n=e.targetTouches[0],je="clientX",Ee="clientY"):0===e.button&&(n=e),n){var o=t.elements.dialog;if(De(o,_e.capture),o.style.left&&(i=parseInt(o.style.left,10)),o.style.top&&(s=parseInt(o.style.top,10)),Te=n[je]-i,Me=n[Ee]-s,t.isModal()?Me+=t.elements.modal.scrollTop:t.isPinned()&&(Me-=document.body.scrollTop),t.get("moveBounded")){
//calc offset
for(var a=o,l=-i,r=-s;l+=a.offsetLeft,r+=a.offsetTop,a=a.offsetParent;);Ne={maxLeft:l,minLeft:-l,maxTop:document.documentElement.clientHeight-o.clientHeight-r,minTop:-r},Ae=P}else Ne=null,Ae=I;
// allow custom `onmove` method
return Qe("onmove",t),Le=!t.isModal()&&t.isPinned(),Ce=t,Ae(n,o),De(document.body,_e.noSelection),!1}}}
/**
         * The actual move handler,  attached to document.body mousemove event.
         *
         * @param {Event} event	DOM event object.
         * 
         * @return {undefined}
         */function S(e){var t;Ce&&("touchmove"===e.type?(e.preventDefault(),t=e.targetTouches[0]):0===e.button&&(t=e),t&&Ae(t,Ce.elements.dialog))}
/**
         * Triggers the end of a move event,  attached to document.body mouseup event.
         * Removes no-selection class from document.body, allowing selection.
         *
         * @return {undefined}
         */function B(){if(Ce){var e=Ce;Ce=Ne=null,Fe(document.body,_e.noSelection),Fe(e.elements.dialog,_e.capture),
// allow custom `onmoved` method
Qe("onmoved",e)}}
/**
         * Resets any changes made by moving the element to its original state,
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function R(e){Ce=null;var t=e.elements.dialog;t.style.left=t.style.top=""}
/**
         * Updates the dialog move behavior.
         *
         * @param {Object} instance The dilog instance.
         * @param {Boolean} on True to add the behavior, removes it otherwise.
         *
         * @return {undefined}
         */function D(e){e.get("movable")?(
// add class
De(e.elements.root,_e.movable),e.isOpen()&&Z(e)):(
//reset
R(e),
// remove class
Fe(e.elements.root,_e.movable),e.isOpen()&&$(e))}
/* Controls moving a dialog around */
//holde the current instance being resized		
/**
         * Helper: sets the element width/height and updates left coordinate if neccessary.
         *
         * @param {Event} event	DOM mousemove event object.
         * @param {Node} element The element being moved.
         * @param {Boolean} pinned A flag indicating if the element being resized is pinned to the screen.
         * 
         * @return {undefined}
         */
function F(e,t,n){for(
//calculate offsets from 0,0
var i=t,s=0,o=0,a,l;s+=i.offsetLeft,o+=i.offsetTop,i=i.offsetParent;);
// determine X,Y coordinates.
l=!0===n?(a=e.pageX,e.pageY):(a=e.clientX,e.clientY);
// rtl handling
var r=Ue();
// if the element being resized has a starting left, maintain it.
// the dialog is centered, divide by half the offset to maintain the margins.
if(r&&(
// reverse X 
a=document.body.offsetWidth-a,
// if has a starting left, calculate offsetRight
isNaN(Pe)||(s=document.body.offsetWidth-s-t.offsetWidth)),
// set width/height
t.style.height=l-o+Be+"px",t.style.width=a-s+Be+"px",!isNaN(Pe)){var c=.5*Math.abs(t.offsetWidth-We);r&&(
//negate the diff, why?
//when growing it should decrease left
//when shrinking it should increase left
c*=-1),t.offsetWidth>We?
//growing
t.style.left=Pe+c+"px":t.offsetWidth>=Se&&(
//shrinking
t.style.left=Pe-c+"px")}}
/**
         * Triggers the start of a resize event, attached to the resize handle element mouse down event.
         * Adds no-selection class to the body, disabling selection while moving.
         *
         * @param {Event} event	DOM event object.
         * @param {Object} instance The dilog instance.
         * 
         * @return {Boolean} false
         */function U(e,t){var n;if(!t.isMaximized()&&("touchstart"===e.type?(e.preventDefault(),n=e.targetTouches[0]):0===e.button&&(n=e),n)){
// allow custom `onresize` method
Qe("onresize",t),Be=(Ie=t).elements.resizeHandle.offsetHeight/2;var i=t.elements.dialog;return De(i,_e.capture),Pe=parseInt(i.style.left,10),i.style.height=i.offsetHeight+"px",i.style.minHeight=t.elements.header.offsetHeight+t.elements.footer.offsetHeight+"px",i.style.width=(We=i.offsetWidth)+"px","none"!==i.style.maxWidth&&(i.style.minWidth=(Se=i.offsetWidth)+"px"),i.style.maxWidth="none",De(document.body,_e.noSelection),!1}}
/**
         * The actual resize handler,  attached to document.body mousemove event.
         *
         * @param {Event} event	DOM event object.
         * 
         * @return {undefined}
         */function X(e){var t;Ie&&("touchmove"===e.type?(e.preventDefault(),t=e.targetTouches[0]):0===e.button&&(t=e),t&&F(t,Ie.elements.dialog,!Ie.get("modal")&&!Ie.get("pinned")))}
/**
         * Triggers the end of a resize event,  attached to document.body mouseup event.
         * Removes no-selection class from document.body, allowing selection.
         *
         * @return {undefined}
         */function Y(){if(Ie){var e=Ie;Ie=null,Fe(document.body,_e.noSelection),Fe(e.elements.dialog,_e.capture),Oe=!0,
// allow custom `onresized` method
Qe("onresized",e)}}
/**
         * Resets any changes made by resizing the element to its original state.
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function q(e){Ie=null;var t=e.elements.dialog;"none"===t.style.maxWidth&&(
//clear inline styles.
t.style.maxWidth=t.style.minWidth=t.style.width=t.style.height=t.style.minHeight=t.style.left="",
//reset variables.
Pe=Number.Nan,We=Se=Be=0)}
/**
         * Updates the dialog move behavior.
         *
         * @param {Object} instance The dilog instance.
         * @param {Boolean} on True to add the behavior, removes it otherwise.
         *
         * @return {undefined}
         */function J(e){e.get("resizable")?(
// add class
De(e.elements.root,_e.resizable),e.isOpen()&&ee(e)):(
//reset
q(e),
// remove class
Fe(e.elements.root,_e.resizable),e.isOpen()&&te(e))}
/**
         * Reset move/resize on window resize.
         *
         * @param {Event} event	window resize event object.
         *
         * @return {undefined}
         */function K(){for(var e=0;e<it.length;e+=1){var t=it[e];t.get("autoReset")&&(R(t),q(t))}}
/**
         * Bind dialogs events
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function V(e){
// if first dialog, hook global handlers
1===it.length&&(
//global
st(Re,"resize",K),st(document.body,"keyup",M),st(document.body,"keydown",j),st(document.body,"focus",N),
//move
st(document.documentElement,"mousemove",S),st(document.documentElement,"touchmove",S),st(document.documentElement,"mouseup",B),st(document.documentElement,"touchend",B),
//resize
st(document.documentElement,"mousemove",X),st(document.documentElement,"touchmove",X),st(document.documentElement,"mouseup",Y),st(document.documentElement,"touchend",Y)),
// common events
st(e.elements.commands.container,"click",e.__internal.commandsClickHandler),st(e.elements.footer,"click",e.__internal.buttonsClickHandler),st(e.elements.reset[0],"focus",e.__internal.resetHandler),st(e.elements.reset[1],"focus",e.__internal.resetHandler),
//prevent handling key up when dialog is being opened by a key stroke.
we=!0,
// hook in transition handler
st(e.elements.dialog,at.type,e.__internal.transitionInHandler),
// modelss only events
e.get("modal")||G(e),
// resizable
e.get("resizable")&&ee(e),
// movable
e.get("movable")&&Z(e)}
/**
         * Unbind dialogs events
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function e(e){
// if last dialog, remove global handlers
1===it.length&&(
//global
ot(Re,"resize",K),ot(document.body,"keyup",M),ot(document.body,"keydown",j),ot(document.body,"focus",N),
//move
ot(document.documentElement,"mousemove",S),ot(document.documentElement,"mouseup",B),
//resize
ot(document.documentElement,"mousemove",X),ot(document.documentElement,"mouseup",Y)),
// common events
ot(e.elements.commands.container,"click",e.__internal.commandsClickHandler),ot(e.elements.footer,"click",e.__internal.buttonsClickHandler),ot(e.elements.reset[0],"focus",e.__internal.resetHandler),ot(e.elements.reset[1],"focus",e.__internal.resetHandler),
// hook out transition handler
st(e.elements.dialog,at.type,e.__internal.transitionOutHandler),
// modelss only events
e.get("modal")||Q(e),
// movable
e.get("movable")&&$(e),
// resizable
e.get("resizable")&&te(e)}
/**
         * Bind modeless specific events
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function G(e){st(e.elements.dialog,"focus",e.__internal.bringToFrontHandler,!0)}
/**
         * Unbind modeless specific events
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function Q(e){ot(e.elements.dialog,"focus",e.__internal.bringToFrontHandler,!0)}
/**
         * Bind movable specific events
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function Z(e){st(e.elements.header,"mousedown",e.__internal.beginMoveHandler),st(e.elements.header,"touchstart",e.__internal.beginMoveHandler)}
/**
         * Unbind movable specific events
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function $(e){ot(e.elements.header,"mousedown",e.__internal.beginMoveHandler),ot(e.elements.header,"touchstart",e.__internal.beginMoveHandler)}
/**
         * Bind resizable specific events
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function ee(e){st(e.elements.resizeHandle,"mousedown",e.__internal.beginResizeHandler),st(e.elements.resizeHandle,"touchstart",e.__internal.beginResizeHandler)}
/**
         * Unbind resizable specific events
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function te(e){ot(e.elements.resizeHandle,"mousedown",e.__internal.beginResizeHandler),ot(e.elements.resizeHandle,"touchstart",e.__internal.beginResizeHandler)}
/**
         * Bind closable events
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function ne(e){st(e.elements.modal,"click",e.__internal.modalClickHandler)}
/**
         * Unbind closable specific events
         *
         * @param {Object} instance The dilog instance.
         *
         * @return {undefined}
         */function ie(e){ot(e.elements.modal,"click",e.__internal.modalClickHandler)}
// dialog API
var//holds the list of used keys.
se=[],
//dummy variable, used to trigger dom reflow.
oe=null,
//holds body tab index in case it has any.
ae=!1,
//condition for detecting safari
le=-1<Re.navigator.userAgent.indexOf("Safari")&&Re.navigator.userAgent.indexOf("Chrome")<0,
//dialog building blocks
re='<div class="ajs-dimmer"></div>',ce='<div class="ajs-modal" tabindex="0"></div>',de='<div class="ajs-dialog" tabindex="0"></div>',ue='<button class="ajs-reset"></button>',me='<div class="ajs-commands"><button class="ajs-pin"></button><button class="ajs-maximize"></button><button class="ajs-close"></button></div>',fe='<div class="ajs-header"></div>',he='<div class="ajs-body"></div>',pe='<div class="ajs-content"></div>',be='<div class="ajs-footer"></div>',ve={primary:'<div class="ajs-primary ajs-buttons"></div>',auxiliary:'<div class="ajs-auxiliary ajs-buttons"></div>'},ge='<button class="ajs-button"></button>',ye='<div class="ajs-handle"></div>',
//common class names
_e={animationIn:"ajs-in",animationOut:"ajs-out",base:"alertify",basic:"ajs-basic",capture:"ajs-capture",closable:"ajs-closable",fixed:"ajs-fixed",frameless:"ajs-frameless",hidden:"ajs-hidden",maximize:"ajs-maximize",maximized:"ajs-maximized",maximizable:"ajs-maximizable",modeless:"ajs-modeless",movable:"ajs-movable",noSelection:"ajs-no-selection",noOverflow:"ajs-no-overflow",noPadding:"ajs-no-padding",pin:"ajs-pin",pinnable:"ajs-pinnable",prefix:"ajs-",resizable:"ajs-resizable",restore:"ajs-restore",shake:"ajs-shake",unpinned:"ajs-unpinned"},ke,xe,He="",ze=0,Oe=!1,we=!1,Ce=null,
//holds the current X offset when move starts
Te=0,
//holds the current Y offset when move starts
Me=0,je="pageX",Ee="pageY",Ne=null,Le=!1,Ae=null,Ie=null,
//holds the staring left offset when resize starts.
Pe=Number.Nan,
//holds the staring width when resize starts.
We=0,
//holds the initial width when resized for the first time.
Se=0,
//holds the offset of the resize handle.
Be=0;return{__init:s,
/**
             * Check if dialog is currently open
             *
             * @return {Boolean}
             */
isOpen:function(){return this.__internal.isOpen},isModal:function(){return this.elements.root.className.indexOf(_e.modeless)<0},isMaximized:function(){return-1<this.elements.root.className.indexOf(_e.maximized)},isPinned:function(){return this.elements.root.className.indexOf(_e.unpinned)<0},maximize:function(){return this.isMaximized()||g(this),this},restore:function(){return this.isMaximized()&&y(this),this},pin:function(){return this.isPinned()||b(this),this},unpin:function(){return this.isPinned()&&v(this),this},bringToFront:function(){return f(null,this),this},
/**
             * Move the dialog to a specific x/y coordinates
             *
             * @param {Number} x    The new dialog x coordinate in pixels.
             * @param {Number} y    The new dialog y coordinate in pixels.
             *
             * @return {Object} The dialog instance.
             */
moveTo:function(e,t){if(!isNaN(e)&&!isNaN(t)){
// allow custom `onmove` method
Qe("onmove",this);var n=this.elements.dialog,i=n,s=0,o=0;
//subtract existing left,top
//calc offset
for(n.style.left&&(s-=parseInt(n.style.left,10)),n.style.top&&(o-=parseInt(n.style.top,10));s+=i.offsetLeft,o+=i.offsetTop,i=i.offsetParent;);
//calc left, top
var a=e-s,l=t-o;
//// rtl handling
Ue()&&(a*=-1),n.style.left=a+"px",n.style.top=l+"px",
// allow custom `onmoved` method
Qe("onmoved",this)}return this},
/**
             * Resize the dialog to a specific width/height (the dialog must be 'resizable').
             * The dialog can be resized to:
             *  A minimum width equal to the initial display width
             *  A minimum height equal to the sum of header/footer heights.
             *
             *
             * @param {Number or String} width    The new dialog width in pixels or in percent.
             * @param {Number or String} height   The new dialog height in pixels or in percent.
             *
             * @return {Object} The dialog instance.
             */
resizeTo:function(e,t){var n=parseFloat(e),i=parseFloat(t),s=/(\d*\.\d+|\d+)%/;if(!isNaN(n)&&!isNaN(i)&&!0===this.get("resizable")){
// allow custom `onresize` method
Qe("onresize",this),(""+e).match(s)&&(n=n/100*document.documentElement.clientWidth),(""+t).match(s)&&(i=i/100*document.documentElement.clientHeight);var o=this.elements.dialog;"none"!==o.style.maxWidth&&(o.style.minWidth=(Se=o.offsetWidth)+"px"),o.style.maxWidth="none",o.style.minHeight=this.elements.header.offsetHeight+this.elements.footer.offsetHeight+"px",o.style.width=n+"px",o.style.height=i+"px",
// allow custom `onresized` method
Qe("onresized",this)}return this},
/**
             * Gets or Sets dialog settings/options 
             *
             * @param {String|Object} key A string specifying a propery name or a collection of key/value pairs.
             * @param {Object} value Optional, the value associated with the key (in case it was a string).
             *
             * @return {undefined}
             */
setting:function(e,t){var i=this,n=m(this,this.__internal.options,function(e,t,n){u(i,e,t,n)},e,t);if("get"===n.op)return n.found?n.value:void 0!==this.settings?m(this,this.settings,this.settingUpdated||function(){},e,t).value:void 0;if("set"===n.op){if(0<n.items.length)for(var s=this.settingUpdated||function(){},o=0;o<n.items.length;o+=1){var a=n.items[o];a.found||void 0===this.settings||m(this,this.settings,s,a.key,a.value)}return this}},
/**
             * [Alias] Sets dialog settings/options 
             */
set:function(e,t){return this.setting(e,t),this},
/**
             * [Alias] Gets dialog settings/options 
             */
get:function(e){return this.setting(e)},
/**
            * Sets dialog header
            * @content {string or element}
            *
            * @return {undefined}
            */
setHeader:function(e){return"string"==typeof e?(qe(this.elements.header),this.elements.header.innerHTML=e):e instanceof Re.HTMLElement&&this.elements.header.firstChild!==e&&(qe(this.elements.header),this.elements.header.appendChild(e)),this},
/**
            * Sets dialog contents
            * @content {string or element}
            *
            * @return {undefined}
            */
setContent:function(e){return"string"==typeof e?(qe(this.elements.content),this.elements.content.innerHTML=e):e instanceof Re.HTMLElement&&this.elements.content.firstChild!==e&&(qe(this.elements.content),this.elements.content.appendChild(e)),this},
/**
             * Show the dialog as modal
             *
             * @return {Object} the dialog instance.
             */
showModal:function(e){return this.show(!0,e)},
/**
             * Show the dialog
             *
             * @return {Object} the dialog instance.
             */
show:function(e,t){if(
// ensure initialization
s(this),this.__internal.isOpen){
// reset move updates
R(this),
// reset resize updates
q(this),
// shake the dialog to indicate its already open
De(this.elements.dialog,_e.shake);var n=this;setTimeout(function(){Fe(n.elements.dialog,_e.shake)},200)}else{if(
// add to open dialogs
this.__internal.isOpen=!0,it.push(this),
// save last focused element
lt.defaults.maintainFocus&&(this.__internal.activeElement=document.activeElement),
// set tabindex attribute on body element this allows script to give it focusable
document.body.hasAttribute("tabindex")||document.body.setAttribute("tabindex",ae="0"),
//allow custom dom manipulation updates before showing the dialog.
"function"==typeof this.prepare&&this.prepare(),V(this),void 0!==e&&this.set("modal",e),
//save scroll to prevent document jump
o(),a(),
// allow custom dialog class on show
"string"==typeof t&&""!==t&&(this.__internal.className=t,De(this.elements.root,t)),
// maximize if start maximized
this.get("startMaximized")?this.maximize():this.isMaximized()&&y(this),x(this),Fe(this.elements.root,_e.animationOut),De(this.elements.root,_e.animationIn),
// set 1s fallback in case transition event doesn't fire
clearTimeout(this.__internal.timerIn),this.__internal.timerIn=setTimeout(this.__internal.transitionInHandler,at.supported?1e3:100),le){
// force desktop safari reflow
var i=this.elements.root;i.style.display="none",setTimeout(function(){i.style.display="block"},0)}
//reflow
oe=this.elements.root.offsetWidth,
// show dialog
Fe(this.elements.root,_e.hidden),
// internal on show event
"function"==typeof this.hooks.onshow&&this.hooks.onshow.call(this),
// allow custom `onshow` method
Qe("onshow",this)}return this},
/**
             * Close the dialog
             *
             * @return {Object} The dialog instance
             */
close:function(){return this.__internal.isOpen&&!1!==Qe("onclosing",this)&&(e(this),Fe(this.elements.root,_e.animationIn),De(this.elements.root,_e.animationOut),
// set 1s fallback in case transition event doesn't fire
clearTimeout(this.__internal.timerOut),this.__internal.timerOut=setTimeout(this.__internal.transitionOutHandler,at.supported?1e3:100),
// hide dialog
De(this.elements.root,_e.hidden),
//reflow
oe=this.elements.modal.offsetWidth,
// remove custom dialog class on hide
void 0!==this.__internal.className&&""!==this.__internal.className&&Fe(this.elements.root,this.__internal.className),
// internal on close event
"function"==typeof this.hooks.onclose&&this.hooks.onclose.call(this),
// allow custom `onclose` method
Qe("onclose",this),
//remove from open dialogs
it.splice(it.indexOf(this),1),this.__internal.isOpen=!1,a()),
// last dialog and tab index was set by us, remove it.
it.length||"0"!==ae||document.body.removeAttribute("tabindex"),this},
/**
             * Close all open dialogs except this.
             *
             * @return {undefined}
             */
closeOthers:function(){return lt.closeAll(this),this},
/**
             * Destroys this dialog instance
             *
             * @return {undefined}
             */
destroy:function(){return this.__internal.isOpen?(
//mark dialog for destruction, this will be called on tranistionOut event.
this.__internal.destroy=function(){Ke(this,s)},
//close the dialog to unbind all events.
this.close()):Ke(this,s),this}}}(),u=function(){
/**
         * Helper: initializes the notifier instance
         *
         */
function i(e){e.__internal||(e.__internal={position:lt.defaults.notifier.position,delay:lt.defaults.notifier.delay},c=document.createElement("DIV"),n(e)),
//add to DOM tree.
c.parentNode!==document.body&&document.body.appendChild(c)}function a(e){e.__internal.pushed=!0,o.push(e)}function l(e){o.splice(o.indexOf(e),1),e.__internal.pushed=!1}
/**
         * Helper: update the notifier instance position
         *
         */function n(e){switch(c.className=d.base,e.__internal.position){case"top-right":De(c,d.top+" "+d.right);break;case"top-left":De(c,d.top+" "+d.left);break;case"top-center":De(c,d.top+" "+d.center);break;case"bottom-left":De(c,d.bottom+" "+d.left);break;case"bottom-center":De(c,d.bottom+" "+d.center);break;default:case"bottom-right":De(c,d.bottom+" "+d.right);break}}
/**
        * creates a new notification message
        *
        * @param  {DOMElement} message	The notifier message element
        * @param  {Number} wait   Time (in ms) to wait before the message is dismissed, a value of 0 means keep open till clicked.
        * @param  {Function} callback A callback function to be invoked when the message is dismissed.
        *
        * @return {undefined}
        */function s(e,t){function n(e,t){t.__internal.closeButton&&"true"!==e.target.getAttribute("data-close")||t.dismiss(!0)}function i(e,t){
// unbind event
ot(t.element,at.type,i),
// remove the message
c.removeChild(t.element)}function s(e){return e.__internal||(e.__internal={pushed:!1,delay:void 0,timer:void 0,clickHandler:void 0,transitionEndHandler:void 0,transitionTimeout:void 0},e.__internal.clickHandler=Ve(e,n),e.__internal.transitionEndHandler=Ve(e,i)),e}function o(e){clearTimeout(e.__internal.timer),clearTimeout(e.__internal.transitionTimeout)}return s({
/* notification DOM element*/
element:e,
/*
                 * Pushes a notification message
                 * @param {string or DOMElement} content The notification message content
                 * @param {Number} wait The time (in seconds) to wait before the message is dismissed, a value of 0 means keep open till clicked.
                 *
                 */
push:function(e,t){if(this.__internal.pushed)return this;var n,i;switch(a(this),o(this),arguments.length){case 0:i=this.__internal.delay;break;case 1:i="number"==typeof e?e:(n=e,this.__internal.delay);break;case 2:n=e,i=t;break}return this.__internal.closeButton=lt.defaults.notifier.closeButton,
// set contents
void 0!==n&&this.setContent(n),
// append or insert
u.__internal.position.indexOf("top")<0?c.appendChild(this.element):c.insertBefore(this.element,c.firstChild),r=this.element.offsetWidth,De(this.element,d.visible),
// attach click event
st(this.element,"click",this.__internal.clickHandler),this.delay(i)},
/*
                 * {Function} callback function to be invoked before dismissing the notification message.
                 * Remarks: A return value === 'false' will cancel the dismissal
                 *
                 */
ondismiss:function(){},
/*
                 * {Function} callback function to be invoked when the message is dismissed.
                 *
                 */
callback:t,
/*
                 * Dismisses the notification message
                 * @param {Boolean} clicked A flag indicating if the dismissal was caused by a click.
                 *
                 */
dismiss:function(e){return this.__internal.pushed&&(o(this),"function"==typeof this.ondismiss&&!1===this.ondismiss.call(this)||(
//detach click event
ot(this.element,"click",this.__internal.clickHandler),
// ensure element exists
void 0!==this.element&&this.element.parentNode===c&&(
//transition end or fallback
this.__internal.transitionTimeout=setTimeout(this.__internal.transitionEndHandler,at.supported?1e3:100),Fe(this.element,d.visible),
// custom callback on dismiss
"function"==typeof this.callback&&this.callback.call(this,e)),l(this))),this},
/*
                 * Delays the notification message dismissal
                 * @param {Number} wait The time (in seconds) to wait before the message is dismissed, a value of 0 means keep open till clicked.
                 *
                 */
delay:function(e){if(o(this),this.__internal.delay=void 0===e||isNaN(+e)?u.__internal.delay:+e,0<this.__internal.delay){var t=this;this.__internal.timer=setTimeout(function(){t.dismiss()},1e3*this.__internal.delay)}return this},
/*
                 * Sets the notification message contents
                 * @param {string or DOMElement} content The notification message content
                 *
                 */
setContent:function(e){if("string"==typeof e?(qe(this.element),this.element.innerHTML=e):e instanceof Re.HTMLElement&&this.element.firstChild!==e&&(qe(this.element),this.element.appendChild(e)),this.__internal.closeButton){var t=document.createElement("span");De(t,d.close),t.setAttribute("data-close",!0),this.element.appendChild(t)}return this},
/*
                 * Dismisses all open notifications except this.
                 *
                 */
dismissOthers:function(){return u.dismissAll(this),this}})}
//notifier api
var r,c,o=[],d={base:"alertify-notifier",message:"ajs-message",top:"ajs-top",right:"ajs-right",bottom:"ajs-bottom",left:"ajs-left",center:"ajs-center",visible:"ajs-visible",hidden:"ajs-hidden",close:"ajs-close"};return{
/**
             * Gets or Sets notifier settings.
             *
             * @param {string} key The setting name
             * @param {Variant} value The setting value.
             *
             * @return {Object}	if the called as a setter, return the notifier instance.
             */
setting:function(e,t){if(
//ensure init
i(this),void 0===t)
//get
return this.__internal[e];
//set
switch(e){case"position":this.__internal.position=t,n(this);break;case"delay":this.__internal.delay=t;break}return this},
/**
             * [Alias] Sets dialog settings/options
             */
set:function(e,t){return this.setting(e,t),this},
/**
             * [Alias] Gets dialog settings/options
             */
get:function(e){return this.setting(e)},
/**
             * Creates a new notification message
             *
             * @param {string} type The type of notification message (simply a CSS class name 'ajs-{type}' to be added).
             * @param {Function} callback  A callback function to be invoked when the message is dismissed.
             *
             * @return {undefined}
             */
create:function(e,t){
//ensure notifier init
i(this);
//create new notification message
var n=document.createElement("div");return n.className=d.message+("string"==typeof e&&""!==e?" ajs-"+e:""),s(n,t)},
/**
             * Dismisses all open notifications.
             *
             * @param {Object} excpet [optional] The notification object to exclude from dismissal.
             *
             */
dismissAll:function(e){for(var t=o.slice(0),n=0;n<t.length;n+=1){var i=t[n];void 0!==e&&e===i||i.dismiss()}}}}(),lt=new e;
/**
     * Default options 
     * @type {Object}
     */
/**
    * Alert dialog definition
    *
    * invoked by:
    *	alertify.alert(message);
    *	alertify.alert(title, message);
    *	alertify.alert(message, onok);
    *	alertify.alert(title, message, onok);
     */
lt.dialog("alert",function(){return{main:function(e,t,n){var i,s,o;switch(arguments.length){case 1:s=e;break;case 2:"function"==typeof t?(s=e,o=t):(i=e,s=t);break;case 3:i=e,s=t,o=n;break}return this.set("title",i),this.set("message",s),this.set("onok",o),this},setup:function(){return{buttons:[{text:lt.defaults.glossary.ok,key:Ze,invokeOnClose:!0,className:lt.defaults.theme.ok}],focus:{element:0,select:!1},options:{maximizable:!1,resizable:!1}}},build:function(){
// nothing
},prepare:function(){
//nothing
},setMessage:function(e){this.setContent(e)},settings:{message:void 0,onok:void 0,label:void 0},settingUpdated:function(e,t,n){switch(e){case"message":this.setMessage(n);break;case"label":this.__internal.buttons[0].element&&(this.__internal.buttons[0].element.innerHTML=n);break}},callback:function(e){if("function"==typeof this.get("onok")){var t=this.get("onok").call(this,e);void 0!==t&&(e.cancel=!t)}}}}),
/**
     * Confirm dialog object
     *
     *	alertify.confirm(message);
     *	alertify.confirm(message, onok);
     *	alertify.confirm(message, onok, oncancel);
     *	alertify.confirm(title, message, onok, oncancel);
     */
lt.dialog("confirm",function(){function s(e){null!==o.timer&&(clearInterval(o.timer),o.timer=null,e.__internal.buttons[o.index].element.innerHTML=o.text)}function t(e,t,n){s(e),o.duration=n,o.index=t,o.text=e.__internal.buttons[t].element.innerHTML,o.timer=setInterval(Ve(e,o.task),1e3),o.task(null,e)}var o={timer:null,index:null,text:null,duration:null,task:function(e,t){if(t.isOpen()){if(t.__internal.buttons[o.index].element.innerHTML=o.text+" (&#8207;"+o.duration+"&#8207;) ",o.duration-=1,-1===o.duration){s(t);var n=t.__internal.buttons[o.index],i=Ge(o.index,n);"function"==typeof t.callback&&t.callback.apply(t,[i]),
//close the dialog.
!1!==i.close&&t.close()}}else s(t)}};return{main:function(e,t,n,i){var s,o,a,l;switch(arguments.length){case 1:o=e;break;case 2:o=e,a=t;break;case 3:o=e,a=t,l=n;break;case 4:s=e,o=t,a=n,l=i;break}return this.set("title",s),this.set("message",o),this.set("onok",a),this.set("oncancel",l),this},setup:function(){return{buttons:[{text:lt.defaults.glossary.ok,key:n,className:lt.defaults.theme.ok},{text:lt.defaults.glossary.cancel,key:Ze,invokeOnClose:!0,className:lt.defaults.theme.cancel}],focus:{element:0,select:!1},options:{maximizable:!1,resizable:!1}}},build:function(){
//nothing
},prepare:function(){
//nothing
},setMessage:function(e){this.setContent(e)},settings:{message:null,labels:null,onok:null,oncancel:null,defaultFocus:null,reverseButtons:null},settingUpdated:function(e,t,n){switch(e){case"message":this.setMessage(n);break;case"labels":"ok"in n&&this.__internal.buttons[0].element&&(this.__internal.buttons[0].text=n.ok,this.__internal.buttons[0].element.innerHTML=n.ok),"cancel"in n&&this.__internal.buttons[1].element&&(this.__internal.buttons[1].text=n.cancel,this.__internal.buttons[1].element.innerHTML=n.cancel);break;case"reverseButtons":!0===n?this.elements.buttons.primary.appendChild(this.__internal.buttons[0].element):this.elements.buttons.primary.appendChild(this.__internal.buttons[1].element);break;case"defaultFocus":this.__internal.focus.element="ok"===n?0:1;break}},callback:function(e){var t;switch(s(this),e.index){case 0:"function"==typeof this.get("onok")&&void 0!==(t=this.get("onok").call(this,e))&&(e.cancel=!t);break;case 1:"function"==typeof this.get("oncancel")&&void 0!==(t=this.get("oncancel").call(this,e))&&(e.cancel=!t);break}},autoOk:function(e){return t(this,0,e),this},autoCancel:function(e){return t(this,1,e),this}}}),
/**
     * Prompt dialog object
     *
     * invoked by:
     *	alertify.prompt(message);
     *	alertify.prompt(message, value);
     *	alertify.prompt(message, value, onok);
     *	alertify.prompt(message, value, onok, oncancel);
     *	alertify.prompt(title, message, value, onok, oncancel);
     */
lt.dialog("prompt",function(){var i=document.createElement("INPUT"),t=document.createElement("P");return{main:function(e,t,n,i,s){var o,a,l,r,c;switch(arguments.length){case 1:a=e;break;case 2:a=e,l=t;break;case 3:a=e,l=t,r=n;break;case 4:a=e,l=t,r=n,c=i;break;case 5:o=e,a=t,l=n,r=i,c=s;break}return this.set("title",o),this.set("message",a),this.set("value",l),this.set("onok",r),this.set("oncancel",c),this},setup:function(){return{buttons:[{text:lt.defaults.glossary.ok,key:n,className:lt.defaults.theme.ok},{text:lt.defaults.glossary.cancel,key:Ze,invokeOnClose:!0,className:lt.defaults.theme.cancel}],focus:{element:i,select:!0},options:{maximizable:!1,resizable:!1}}},build:function(){i.className=lt.defaults.theme.input,i.setAttribute("type","text"),i.value=this.get("value"),this.elements.content.appendChild(t),this.elements.content.appendChild(i)},prepare:function(){
//nothing
},setMessage:function(e){"string"==typeof e?(qe(t),t.innerHTML=e):e instanceof Re.HTMLElement&&t.firstChild!==e&&(qe(t),t.appendChild(e))},settings:{message:void 0,labels:void 0,onok:void 0,oncancel:void 0,value:"",type:"text",reverseButtons:void 0},settingUpdated:function(e,t,n){switch(e){case"message":this.setMessage(n);break;case"value":i.value=n;break;case"type":switch(n){case"text":case"color":case"date":case"datetime-local":case"email":case"month":case"number":case"password":case"search":case"tel":case"time":case"week":i.type=n;break;default:i.type="text";break}break;case"labels":n.ok&&this.__internal.buttons[0].element&&(this.__internal.buttons[0].element.innerHTML=n.ok),n.cancel&&this.__internal.buttons[1].element&&(this.__internal.buttons[1].element.innerHTML=n.cancel);break;case"reverseButtons":!0===n?this.elements.buttons.primary.appendChild(this.__internal.buttons[0].element):this.elements.buttons.primary.appendChild(this.__internal.buttons[1].element);break}},callback:function(e){var t;switch(e.index){case 0:this.settings.value=i.value,"function"==typeof this.get("onok")&&void 0!==(t=this.get("onok").call(this,e,this.settings.value))&&(e.cancel=!t);break;case 1:"function"==typeof this.get("oncancel")&&void 0!==(t=this.get("oncancel").call(this,e))&&(e.cancel=!t),e.cancel||(i.value=this.settings.value);break}}}}),
// CommonJS
"object"==typeof module&&"object"==typeof module.exports?module.exports=lt:"function"==typeof define&&define.amd?define([],function(){return lt}):Re.alertify||(Re.alertify=lt)}("undefined"!=typeof window?window:this);