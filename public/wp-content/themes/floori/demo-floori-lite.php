<?php
/**
 * Template Name: Floori Lite demo
 * Template Post Type: post, page, product
 *
  * @package floori
 */

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <iframe class="lite-demo" src="http://188.68.225.139:1234/preview/74513061d8a22e476bbf21b7714d8e772e547225825f1e4510de0e5f9c3e977f90e170b7665bd7823fdbc31683d95d0ad977"></iframe>

    </main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
