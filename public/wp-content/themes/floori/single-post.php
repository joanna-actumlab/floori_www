<?php
/*
* Template Name: Single Post Template
* Template Post Type: post, page, product
*/
 
get_header();
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <div class="header-blog" id="home">
                <div class="brand">
                    <img id="flori-logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/Frame.png" alt="floori logo" />
                    <span class="brand-bg"></span>
                </div>
                <div class="header-bg">
                </div>
                <!-- .header-bg END -->
            </div>

            <section id="single-post-page" class="w-1200">

                <?php
					display_single_post();
				?>

            </section>

        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
		get_footer();
	?>
