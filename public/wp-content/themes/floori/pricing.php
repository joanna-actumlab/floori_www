<?php
/**
 *  Template name: Pricing
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package floori
 */
get_header();
?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="header-pricing" id="home">
                <div class="brand">
                    <img id="flori-logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/Frame.png" alt="floori logo" />
                    <span class="brand-bg"></span>
                </div>
                <div class="header-bg">
                </div>
                <!-- .header-bg END -->
            </div>
            <div id="posts-home-page" class="w-1200 text-c text-dark">
                <h1 class="text-dark text-c">Interested in our product?</h1>
                <container class="feat-container">
                    <p>Pricing calculated individually.</p>
                    <p>Contact us: hello@actumlab.com </p>
                    <p>or use contact form below.</p>
                </container>
                <span style="display:block; margin: 1em auto 0 auto; width: 50%; height: 1px; background-color: #dddddd;"></span>
                <section id="kontakt">
                    <h1>Contact us</h1>
                    <form action="" id="contactform" method="POST">
                        <input class="ios-nochange" aria-label="Name field" type="text" name="name" id="name" placeholder="First Name" required>
                        <input class="ios-nochange" aria-label="email field" type="email" name="email" id="email" placeholder="E-mail" required>
                        <input class="ios-nochange" aria-label="company name field" type="text" name="company" id="company" placeholder="Company Name" required>
                        <textarea aria-label="your message field" name="message" id="message" rows="10" cols="25" placeholder="Type your message here" required></textarea>
                        <label>
                            <input aria-label="confirm terms box" type="checkbox" id="checkbox" checked required>I agree to Terms & Conditions</input>
                        </label>
                        <container>
                            <a href="<?php echo get_page_link( get_page_by_title( 'Terms of use' )->ID ); ?>">Privacy Policy</a>
                            <a href="<?php echo get_page_link( get_page_by_title( 'Privacy Policy' )->ID ); ?>">Terms of use</a>
                        </container>
                        <input aria-label="submit form button" type="submit" name="submit" id="submit" value="Send" class="btn-send ios-nochange">
                    </form>
                </section>
            </div>
        </main>
        <!-- #main END -->
    </div>
    <!-- .content-area END -->
    <?php
            get_footer();
        ?>
