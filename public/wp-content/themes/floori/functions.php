<?php
/**
 * floori functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package floori
 */

if ( ! function_exists( 'floori_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function floori_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on floori, use a find and replace
		 * to change 'floori' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'floori', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'floori' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'floori_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'floori_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function floori_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'floori_content_width', 640 );
}
add_action( 'after_setup_theme', 'floori_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function floori_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'floori' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'floori' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'floori_widgets_init' );


/**
 * 
 * This is custom function to count logo files in dir: '.\assets\img\logo', wrap them in <div> and echo out.
 * 
 */
/* 
	Aby dodac nowe logo wystarczy wrzucic plik o dowolnej nazwie do : 
	C:\Users\MichalS\Local Sites\flooriwordpress\app\public\wp-content\themes\floori\assets\img 
*/
function display_available_logos($group){
 /******* count files *******/
	switch ($group)
		{
		case "customers":
		$sciezka = get_template_directory().'/assets/img/customers/grayscale/';
		$sciezka2 = get_template_directory_uri().'/assets/img/customers/grayscale/'.$ende;
		break;

		case "floors":
		$sciezka = get_template_directory().'/assets/img/floors/';
		$sciezka2 = get_template_directory_uri().'/assets/img/floors/'.$ende;
		break;
		}

	$licznikPlikow = 0;
	$katalog = opendir($sciezka);
	// exploring catalog and iterating the counter ( amount of files counter)
	while($plik = readdir($katalog))
	{
	   //weryfikacja czy rzeczywiście dany element to plik
	   if ($plik<>'.' && $plik<>'..' && !is_dir($sciezka.$plik))
	   {
		   //zwiększenie licznika plików
		   $licznikPlikow++;
	   }
	}
	closedir($katalog);
 /******* List of files *******/
	$tablica;
	// Open a directory, and read its contents
	if (is_dir($sciezka)){
  		if ($dh = opendir($sciezka)){
	  		$i = 0;
    		while (($file = readdir($dh)) !== false){
				if ($file != "." && $file != ".." && $file != "loga testowe"){
				$tablica[$i] = $file;
				$i++;
				}
    		}
    		closedir($dh);
  		}
	}
 /******* create div's for slick *******/		
	if ($licznikPlikow){
		for ($i = 0; $i < $licznikPlikow; $i++) {
    		echo "<div class='slider-img' style='background-image: url($sciezka2$tablica[$i])'></div>";
		}
	}
	else {
		echo "<div>There are no available logos at the moment /assets/img/.... </div>";
	}
}
/*
* This function will display 3 most recent posts if has /.thumbnail/(featured image) applied 
* https://wordpress.stackexchange.com/questions/245769/get-recent-posts-with-thumbnail 
*/
function display_recent_posts($how_many_posts) {
	if (!isset($how_many_posts)){
		$count_posts = wp_count_posts();
		$how_many_posts = $count_posts->publish;
	}

	$args = array( "posts_per_page" => "$how_many_posts" );
	$recent_posts = new WP_Query($args);

	while( $recent_posts->have_posts() ) :  
			$recent_posts->the_post();
		echo "<div class='feat-box col-3'>";
			 if ( has_post_thumbnail() ) {
				 $nazwa_postu = get_the_title();
				 $permalink = get_permalink();
				 echo "<a class='title-permalink' href='$permalink'>";
				 echo "<div class='feat-icon'>"; the_post_thumbnail('post-thumbnail'); echo "</div>";
				 echo "<h3 class='text-c'> $nazwa_postu </h3>"; 
				 echo "</a>";
				}
		echo "</div>"; /* end of ..feat-box col-3 */
	endwhile;
wp_reset_postdata(); /* reset post data so that other queries/loops work */
}
/**
 * Below function displays single post ( title, time, and content ), 
 * earlier clicked the permalink [ site to display -> single-post.php ]
 */
function display_single_post() {
		while ( have_posts() ) :
			echo "<div class='single-post-content w-1200'>";
				the_post();
				echo "<p class='post-title'>"; the_title(); echo "</p>";
				echo "<div class='post-content'>";the_content(); echo "</div>";
			echo "</div>";
		endwhile;
		wp_reset_postdata();
}
/**
 * Below function displays all available post ( title, time, and content ), 
 * earlier clicked the permalink [ site to display -> single-post.php ]
 */
function display_all_published_posts() {
  	$count_posts = wp_count_posts();
	$published_posts = $count_posts->publish;
	
	$args = array( "posts_per_page" => "$published_posts" );
	$recent_posts = new WP_Query($args);

	while( $recent_posts->have_posts() ) :  
			$recent_posts->the_post();
	echo "<li>";
	echo "<div class='feat-box col-3'>";
			 if ( has_post_thumbnail() ) {
				 $nazwa_postu = get_the_title();
				 $permalink = get_permalink();
				 echo "<a class='title-permalink' href='$permalink'>";
				 echo "<div class='feat-icon'>"; the_post_thumbnail('post-thumbnail'); echo "</div>";
				 echo "<h3 class='text-c'> $nazwa_postu </h3>"; 
				 echo "<h3 class='text-c'>"; the_time('Y/m/d'); echo "<h3 class='text-c'>";
				 echo "</a>";
				}
	echo "</div>"; /* end of ..feat-box col-3 */
	echo '</li>';
	endwhile;
wp_reset_postdata(); /* reset post data so that other queries/loops work */
}

/**
 * This include custom jQuery library theme default 1.1.2 or so
 */

 
function shapeSpace_include_custom_jquery() {

	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.3.1.min.js', array(), null, true);

}
add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');

/**
 * Add Favicons
 */
function myfavicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_template_directory_uri().'/assets/img/favicon.ico" />';
	echo '<link rel="icon" type="image/png" sizes="16x16" href="'.get_template_directory_uri().'/assets/img/favicon-16x16.png" />';
	echo '<link rel="icon" type="image/png" sizes="32x32" href="'.get_template_directory_uri().'/assets/img/favicon-32x32.png" />';
}
add_action('wp_head', 'myfavicon');
/**
 * Enqueue scripts and styles.
 */

function floori_scripts() {
	wp_enqueue_style( 'floori-style', get_template_directory_uri() . '/css/custom.css' );
    	wp_enqueue_style( 'aos-style', get_template_directory_uri() . '/css/aos.css' );
	wp_enqueue_style( 'slick-style-1', get_template_directory_uri() . '/slick/slick.css' );
	wp_enqueue_style( 'slick-style-2', get_template_directory_uri() . '/slick/slick-theme.css' );
	wp_enqueue_style( 'font-awesome-free-4', 'https://use.fontawesome.com/releases/v5.3.1/css/all.css' );
	wp_enqueue_style( 'google-api-300', 'https://fonts.googleapis.com/css?family=Rajdhani:300' );
	wp_enqueue_style( 'google-api-200', 'https://fonts.googleapis.com/css?family=Catamaran:200' );
	wp_enqueue_style('alertify-css', get_template_directory_uri() . '/alertifyjs/css/alertify.min.css');
	wp_enqueue_style('alertify-css-default', get_template_directory_uri() . '/alertifyjs/css/themes/default.min.css');
	wp_enqueue_style('alertify-css-semantic', get_template_directory_uri() . '/alertifyjs/css/themes/semantic.css');
	wp_enqueue_script( 'cookiez', 'https://ciasteczka.eu/cookiesEU-latest.min.js', array(), null, true);
	wp_enqueue_script( 'floori-custom-functions', get_template_directory_uri() . '/js/core.js',array('jquery'), '1.0', true ); 
	wp_localize_script('floori-custom-functions', 'myScript', array('theme_directory' => get_template_directory_uri() ));
	wp_enqueue_script( 'floori-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script('slick-js', get_template_directory_uri() . '/slick/slick.min.js', array(), null, true);
	wp_enqueue_script('alertify-js', get_template_directory_uri() . '/alertifyjs/alertify.min.js', array(), null, true);
	wp_enqueue_script( 'font-awesome-js', 'https://use.fontawesome.com/releases/v5.3.1/js/all.js', array(), null, true);
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
    
    wp_enqueue_script( 'menu', get_template_directory_uri() . '/js/menu.js', array(), null, true );
        wp_enqueue_script( 'scroll', get_template_directory_uri() . '/js/scroll.js', array(), null, true );
            wp_enqueue_script( 'aos-animation-library', get_template_directory_uri() . '/js/aos.js', array(), null, true );
}
add_action( 'wp_enqueue_scripts', 'floori_scripts' );


/**
* Implement the Custom Header feature.
*/
require get_template_directory() . '/inc/custom-header.php';

/**
* Custom template tags for this theme.
*/
require get_template_directory() . '/inc/template-tags.php';

/**
* Functions which enhance the theme by hooking into WordPress.
*/
require get_template_directory() . '/inc/template-functions.php';

/**
* Customizer additions.
*/
require get_template_directory() . '/inc/customizer.php';

/**
* Load Jetpack compatibility file.
*/
if ( defined( 'JETPACK__VERSION' ) ) {
require get_template_directory() . '/inc/jetpack.php';
}
