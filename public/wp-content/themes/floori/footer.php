<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package floori
 */

?>

</div>
<!-- #content -->

<footer id="colophon" class="site-footer">
    <?php if( have_rows('footer',6) ): ?>
    <div id="footer">
        <?php while ( have_rows('footer',6) ) : the_row(); ?>

        <div class="social-media-icons w-1200">
            <a href="https://facebook.com/flooriAR" aria-label="facebook icon as link"> <i class="fab fa-facebook"></i></a>
            <a href="https://instagram.com/floori_ar" aria-label="instagram icon as link"> <i class="fab fa-instagram"></i></a>
            <a href="https://twitter.com/floori_ar" aria-label="twitter icon as link"> <i class="fab fa-twitter "></i></a>
            <a href="https://www.youtube.com/channel/UCoKYQw6508yjZVAHCw-JQ9w" aria-label="youtube icon as link"> <i class="fab fa-youtube "></i></a>
        </div>
        <div class="copyright">
            <?php the_sub_field('footer-slogan',6); ?>
        </div>
        <?php if( have_rows('footer-link-wrapper',6) ): ?>
        <div class="menu-footer">

            <ul>

                <?php while ( have_rows('footer-link-wrapper',6) ) : the_row(); ?>
                <li>
                    <?php if( have_rows('footer-links',6) ): ?>
                    <?php while ( have_rows('footer-links',6) ) : the_row(); ?>
                    <a href="<?php the_sub_field('footer-link',6); ?>">
                        <?php the_sub_field('footer-link-label',6); ?>
                    </a>
                    <?php endwhile;?>
                    <?php endif;?>
                </li>

                <?php endwhile;?>
            </ul>

        </div>
        <?php endif;?>
        <?php endwhile;?>
    </div>
    <?php endif;?>

    <a href="#home">
        <div class="arrow-up">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-up.svg" alt="arrow-up.svg">
        </div>
    </a>

</footer>
<!-- #colophon -->
</div>
<!-- #page -->

<?php wp_footer(); ?>


<script>
    AOS.init();

</script>
</body>

</html>
