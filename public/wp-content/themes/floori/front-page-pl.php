<?php

/**

 * Template name: Front-Page-Pl

 *

 * This is the template that displays all pages by default.

 * Please note that this is the WordPress construct of pages

 * and that other 'pages' on your WordPress site may use a

 * different template.

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *

 * @package floori

 */



get_header();

?>



<div id="primary" class="content-area">

    <main id="main" class="site-main">



        <div class="header" id="home">

            <div class="brand">

                <img id="flori-logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/Frame.png" alt="floori logo" />

                <span class="brand-bg"></span>

            </div>





            <div class="title">

                <h1>Floori</h1>
                <p>to rewolucyjna aplikacja mobilna, która pozwoli zaoszczędzić twojemu biznesowi czas oraz sprzedać więcej produktów</p>

                <a>

                    <div class="empty-div">

                        <a class="badge go-to-video" href="#how-it-works">Zobacz jak działa</a>

                    </div>

                </a>

            </div>

            <div class="ipad-gif">
            </div>

            <div class="header-bg"></div>

        </div>



        <section id="features" class="w-1200">



            <h1 class="text-c">Floori - narzędzie sprzedaży podłóg: pomoże Tobie sprzedać więcej,

                <span style="color: #f15a24;">szybciej</span>

            </h1>



            <container class="feat-container">

                <div class="feat-box col-3">



                    <div class="feat-icon">

                        <p>

                            <i class="far fa-clock"></i>

                        </p>

                    </div>

                    <h3 class="text-c">Widok w czasie rzeczywistym</h3>

                    <p>Renderowanie w czasie rzeczywistym Twoich produktów - bez konieczności zaznaczania markera lub przestrzeni!</p>

                </div>

                <div class="feat-box col-3">

                    <div class="feat-icon">

                        <p>

                            <i class="fas fa-th"></i>

                        </p>

                    </div>

                    <h3 class="text-c">Kompletny katalog produktów</h3>

                    <p>

                        Twój cały ekwipunek w dłoni - bez kosztownych papierów i katalogów!

                    </p>

                </div>

                <div class="feat-box col-3">

                    <div class="feat-icon">

                        <p>

                            <i class="fas fa-shopping-basket"></i>

                        </p>

                    </div>

                    <h3 class="text-c">Potężne narzędzie sprzedaży</h3>

                    <p>Floori znacznie zwiększa wielkość sprzedaży, ułatwiając klientom podejmowanie decyzji</p>

                </div>

            </container>

        </section>





        <section id="how-it-works">

            <h1 class="text-dark text-c">Jak to działa?</h1>

            <div class="video">

                <iframe title="vide explaining the work of floori app" width="560" height="315" src="" data-src="https://www.youtube.com/embed/tYnLQDVE0Dg?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

            </div>

            <p class="text-under-movie   text-dark">Floori wykorzystuje specjalne urządzenie o nazwie Sensor Structure 3D, które ma możliwość przekształcenia funkcjonalności iPada lub iPhone'a! Dzięki naszej aplikacji możesz dostosować kierunek struktury podłogi i tworzyć katalogi produktów na naszej platformie - wszystko, czego potrzeba, aby zacząć pracować wydajniej!</p>

        </section>

        <section id="our-floors">

            <h1 id="toggle-flooring" class="text-dark text-c">Poznaj producentów, którzy już z nami współpracują</h1>

            <div class="responsive w-1200">

                <?php

                            display_available_logos("floors");

                        ?>

            </div>

        </section>

        <section id="preview-in-seconds">

            <div class="howto-icons w-1200 ">

                <h1 class="text-c ">Podgląd w kilka sekund</h1>

                <div class="icon-wrapper ">

                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Floori-howto-1.svg" alt="Floori-howto-1.svg" />

                    <h3>1.</h3>

                    <p>Zaloguj się do swojego konta na naszej platformie Floori</p>

                </div>

                <div class="icon-wrapper ">

                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Floori-howto-2.svg" alt="Floori-howto-2.svg" />

                    <h3>2.</h3>

                    <p>Załaduj tekstury podłogi (.jpg .bmp .png files)</p>

                </div>

                <div class="icon-wrapper ">

                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Floori-howto-3.svg" alt="Floori-howto-3.svg" />

                    <h3>3.</h3>

                    <p>Otwórz aplikację Floori na urządzeniu mobilnym Apple z podłączonym czujnikiem 3D Structure</p>

                </div>

                <div class="icon-wrapper ">

                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Floori-howto-4.svg" alt="Floori-howto-4.svg" />

                    <h3>4.</h3>

                    <p>Zobacz, jak twoje opcje podłóg dokładnie leżą na podłodze dzięki technologii Augmented Reality</p>

                </div>

            </div>

        </section>

        <!--

                <section id="parallax-scroll">

                    <div class="parallax">



                    </div>

                </section>
-->


        <section id="technology" class="w-1200">

            <h1 class="text-c">Aby użyć Floori, potrzebujesz ...</h1>

            <div class="box-container">

                <div class="ipad-wrapper">

                    <a href="https://www.apple.com/ipad/">

                        <div class="ipad box"></div>

                        <h3>Apple iPad</h3>

                    </a>

                </div>



                <div class="plus">

                    <p>&#43;</p>

                </div>



                <div class="structure-wrapper">

                    <a href="https://store.structure.io/store">

                        <div class="structure box"></div>

                        <h3>Structure</h3>

                    </a>

                </div>



                <div class="plus">

                    <p>&#43;</p>

                </div>



                <div class="floori-wrapper">

                    <a href="https://itunes.apple.com/us/app/floori-ar-floors-carpeting/id1321337280?mt=8">

                        <div class="floori box"></div>

                        <h3>Floori App</h3>

                    </a>

                </div>

            </div>

            <a href="https://structure.io/uses" class="btn-send">Odkryj inne niesamowite zastosowania Structure Sensor 3D</a>

        </section>

        <section id="our-clients">

            <h1 class="text-dark text-c">Zasilamy ich sprzedaż!</h1>

            <div class="responsive w-1200">

                <?php

                            display_available_logos("customers");

                        ?>

            </div>

        </section>



        <section id="posts-front-page">

            <h1 class="text-c">Najnowsze posty</h1>

            <container class="feat-container w-1200">

                <?php

                            display_recent_posts(3);

                        ?>

            </container>

        </section>



        <section class="social-media">

            <h1>Chcesz się dowiedzieć więcej?</h1>

            <p>Śledź nas w mediach społecznościowych, aby być na bieżąco!</p>



            <div class="w-1200 youtube-wrapper">

                <div class="video-frame">

                    <iframe title="how to use application" width="560" height="315" src="" data-src="https://www.youtube.com/embed/5qDlRNwI2WM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                </div>

                <div class="video-frame">

                    <iframe title="changing textures on the flow" width="560" height="315" src="" data-src="https://www.youtube.com/embed/zzV9MxvUwhM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                </div>

                <div class="video-frame">

                    <iframe title="live preview" width="560" height="315" src="" data-src="https://www.youtube.com/embed/SyxOR5qK8_E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                </div>

            </div>



            <div class="social-media-icons w-1200">

                <a href="https://facebook.com/flooriAR" aria-label="facebook icon as link"> <i class="fab fa-facebook fa-2x"></i></a>

                <a href="https://instagram.com/floori_ar" aria-label="instagram icon as link"> <i class="fab fa-instagram fa-2x"></i></a>

                <a href="https://twitter.com/floori_ar" aria-label="twitter icon as link"> <i class="fab fa-twitter fa-2x"></i></a>

                <a href="https://www.youtube.com/channel/UCoKYQw6508yjZVAHCw-JQ9w" aria-label="youtube icon as link"> <i class="fab fa-youtube fa-2x"></i></a>

            </div>

        </section>

        <section id="contact">
            <div class="w-1200">

                <div id="map">
                    <h1 class="contact-header">Skontaktuj sie z nami</h1>
                    <p> Pytania, pomysły lub opinie? Wyślij nam wiadomość, a my skontaktujemy się z Tobą najszybciej, jak to możliwe!
                        <p>
                            <p> Austin, USA + 1 616 485 6925 </p>
                            <p> Warszawa, Polska + 48 790 495 310 </p>
                            <p> Gdansk, Polska + 48 661 351 015 </p>
                            <p> Dublin, Irlandia + 48 790 495 310 </p>
                            <p> Wellington, Nowa Zelandia + 64 226 27 0199 </p>
                            <!-- <div class="floori-map"  ></div>  TU MAPKA ORYGINALNA OD ASI GRAFIKA-->
                            <div id="mapdiv"></div>
                </div>

                <form action="" id="contact-form" method="POST">

                    <input class="ios-nochange" aria-label="Name field" type="text" name="name" id="name" placeholder="Imie" required>
                    <input class="ios-nochange" aria-label="email field" type="email" name="email" id="email" placeholder="E-mail" required>
                    <input class="ios-nochange" aria-label="company name field" type="text" name="company" id="company" placeholder="Nazwa firmy" required>
                    <textarea aria-label="your message field" name="message" id="message" rows="10" cols="25" placeholder="Twoja wiadomość" required></textarea>

                    <label>
                        <input aria-label="confirm terms box" type="checkbox" id="checkbox" checked required>Zgadzam się z warunkami</input>
                    </label>
                    <container>
                        <a href="<?php echo get_page_link( get_page_by_title( 'Terms of use' )->ID ); ?>">Polityka prywatności</a>
                        <a href="<?php echo get_page_link( get_page_by_title( 'Privacy Policy' )->ID ); ?>">Warunki</a>
                    </container>

                    <input aria-label="submit form button" type="submit" name="submit" id="submit" value="Send" class="btn-send ios-nochange">

                </form>
            </div>
        </section>

    </main>

    <!-- #main -->

</div>

<!-- #primary -->


<?php
if (get_locale() == 'pl_PL') {

            get_footer('pl');}
else{
    get_footer();
}

        ?>
