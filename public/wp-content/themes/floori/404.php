<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package floori
 */
get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">


        <div class='w-1200'>
            <div class='post-content'>
                <div class="error-img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/404.svg');"></div>
                <h3> That page cannot be found. </h3>
                <a href="https://floori.io" class="button btn-orange m-auto">
                    <p>Go back</p>
                </a>
            </div>
        </div>

    </main>
    <!-- #main -->
</div>
<!-- #primary -->

<?php
if (get_locale() == 'pl_PL') {

            get_footer('pl');}
else{
    get_footer();
}

        ?>
