<?php
/**
 * Template name: logoCounter
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package floori
 */
?>
<?php
//ścieżka do katalogu
$sciezka = get_template_directory().'/assets/img/logo/';
 
//ustawienie domyślnej wartości licznika plików
$licznikPlikow = 0;
 
//otwarcie katalogu
$katalog = opendir($sciezka);
 
//przejście po elementach katalogu
while($plik = readdir($katalog))
{
   //weryfikacja czy rzeczywiście dany element to plik
   if ($plik<>'.' && $plik<>'..' && !is_dir($sciezka.$plik))
   {
       //zwiększenie licznika plików
       $licznikPlikow++;
   }
}
 
//zamknięcie katalogu
closedir($katalog);
 
//wyświetlenie komunikatu
echo 'W katalogu znaleziono '.$licznikPlikow.' plików';

echo get_template_directory()."/assets/img/logo/";
?>
