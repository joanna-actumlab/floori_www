<?php

/**

 *Template Name: Front Page PL example
 *
 * Template Post Type: post, page, product
  *
 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *


 * @package floori

 */



get_header();

?>



    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="header" id="home">
                <div class="header-wrapper">
                    <div class="header-gif ipad-gif">
                    </div>
                    <div class="title">
                        <h1>Floori</h1>
                        <p>To rewolucyjne oprogramowanie, które pomaga firmom z branży podłogowej oszczędzać czas i sprzedawać więcej produktów.</p>

                        <div class="header-buttons" data-aos="fade-up">

                            <a class="button" href="/floori-lite">
                                <p>Floori Lite</p>
                            </a>
                            <a class="button" href="/floori-professional">
                                <p>Floori Professional</p>
                            </a>
                        </div>

                    </div>
                </div>
                <div class="header-bg"></div>
            </div>

            <section id="features" class="w-1200" data-aos="fade-up" data-aos-delay="200">
                <h2 class="text-c">Floori - pomaga zrealizować więcej zleceń,
                    <span style="color: #f15a24;">szybciej</span>
                </h2>
                <container class="feat-container">
                    <div class="feat-box">
                        <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php echo get_sub_field('feat-img'); ?>');">

                        </div>
                        <h3 class="text-c">Podgląd w czasie rzeczywistym</h3>
                        <p>Podgląd w czasie rzeczywistym Twoich produktów – bez zbędnych znaczników i zaznaczania przestrzeni!</p>
                    </div>
                    <div class="feat-box">
                        <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/grid.svg');">

                        </div>
                        <h3 class="text-c">Cały katalog produktów na wyciągnięcie ręki</h3>
                        <p>
                            koniec z kosztownych papierowych renderów i katalogów!
                        </p>
                    </div>
                    <div class="feat-box">
                        <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/trending-up.svg');">

                        </div>
                        <h3 class="text-c">Potężne narzędzie sprzedażowe</h3>
                        <p>Floori znacząco zwiększa sprzedaż oraz konwersję, ułatwiając klientom podjęcie decyzji</p>
                    </div>
                </container>
            </section>

            <!--    <section id="our-floors">
                <h1 id="toggle-flooring" class="text-dark text-c">Meet the manufacturers already working with us</h1>
                <div class="responsive w-1200">
                    <?php
                            display_available_logos("floors");
                        ?>
                </div>
            </section>-->


            <section id="options" class="options" data-aos="fade-up">
                <h2 class="text-c">Wybierz swoje Floori!</h2>
                <div class="options-wrapper">
                    <div class="option-lite">
                        <h2 class="text-c">Floori Lite</h2>
                        <ul>
                            <li data-aos="fade-left"><i></i>
                                <p>Dostępne na przeglądarki oraz urządzenia mobilne
                                </p>
                            </li>
                            <li data-aos="fade-left"><i></i>
                                <p>Łatwe wdrożenie na Twoją stronę internetową oraz e-commerce</p>
                            </li>
                            <li data-aos="fade-left"><i></i>
                                <p>Automatyczne wykrywanie powierzchni oraz perspektywy </p>
                            </li>
                            <li data-aos="fade-left"><i></i>
                                <p>Wyposażone w Sztuczną Inteligencję (AI)</p>
                            </li>
                        </ul>
                        <a href="/floori-lite">
                            <div class="button btn-orange m-auto">
                                <p>Dowiedz się więcej...</p>
                            </div>
                        </a>
                    </div>
                    <div class="option-pro">
                        <h2 class="text-c">Floori Professional</h2>
                        <ul>
                            <li data-aos="fade-left"><i></i>
                                <p>Dostępne jako aplikacja na urządzenia iOS</p>
                            </li>
                            <li data-aos="fade-left"><i></i>
                                <p>Podgląd i wizualizacje w czasie rzeczywistym</p>
                            </li>
                            <li data-aos="fade-left"><i></i>
                                <p>Łatwe i dokładne tworzenie rzutów podłóg </p>
                            </li>
                            <li data-aos="fade-left"><i></i>
                                <p>Błyskawiczne tworzenie ofert dla klientów </p>
                            </li>
                        </ul>
                        <a href="/floori-professional ">
                            <div class=" button btn-orange m-auto">
                                <p>Dowiedz się więcej...</p>
                            </div>
                        </a>
                    </div>
                </div>


            </section>
            <!--
            <section id="posts-front-page">
                <h1 class="text-c">Recent posts</h1>
                <container class="feat-container w-1200">
                    <?php
                            display_recent_posts(3);
                        ?>
                </container>
            </section>-->
            <section id="our-clients" data-aos="fade-up">
                <h1 class="text-dark text-c">Napędzamy ich sprzedaż!</h1>
                <div class="grid-wrapper w-1200">
                    <!--
                <?php
                            display_available_logos("customers");
                        ?>
-->
                    <div class="customer-logo" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/customers/grayscale/logo_1.png');"></div>

                    <div class="customer-logo" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/customers/grayscale/logo_2.png');"></div>

                    <div class="customer-logo" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/customers/grayscale/logo_3.png');"></div>

                    <div class="customer-logo" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/customers/grayscale/logo_4.png');"></div>

                    <div class="customer-logo" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/customers/grayscale/logo_5.png');"></div>

                    <div class="customer-logo" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/customers/grayscale/logo_6.png');"></div>


                </div>
            </section>
            <section class="social-media" data-aos="fade-up">
                <h1>Chcesz dowiedzieć się więcej?</h1>
                <div class="social-media-icons w-1200">
                    <a href="https://facebook.com/flooriAR" aria-label="facebook icon as link"> <i class="fab fa-facebook fa-2x"></i></a>
                    <a href="https://instagram.com/floori_ar" aria-label="instagram icon as link"> <i class="fab fa-instagram fa-2x"></i></a>
                    <a href="https://twitter.com/floori_ar" aria-label="twitter icon as link"> <i class="fab fa-twitter fa-2x"></i></a>
                    <a href="https://www.youtube.com/channel/UCoKYQw6508yjZVAHCw-JQ9w" aria-label="youtube icon as link"> <i class="fab fa-youtube fa-2x"></i></a>
                </div>
                <div class="w-1200 youtube-wrapper">
                    <div class="video-frame" data-aos="fade-up">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/5qDlRNwI2WM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="video-frame" data-aos="fade-up">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/zzV9MxvUwhM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="video-frame" data-aos="fade-up">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/SyxOR5qK8_E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </section>

            <section id="contact" class="contact">
                <div class="contact-wrapper">
                    <div class="contact-info">
                        <h2 data-aos="fade-up">Gdzie nas znaleźć?</h2>
                        <div class="contact-numbers">
                            <p><span data-aos="fade-up">Austin, USA</span><br> <span data-aos="fade-up">Warszawa, Polska</span><br> <span data-aos="fade-up">Gdańsk, Polska</span><br> <span data-aos="fade-up">Dublin, Irlandia</span><br> <span data-aos="fade-up">Wellington, Nowa Zealandia</span></p>
                            <p><a href="tel:+1-616-485-6925" data-aos="fade-up">+ 1 616 485 6925</a><br>
                                <a href="tel:+48-790-495-310" data-aos="fade-up">+ 48 790 495 310</a><br>
                                <a href="tel:+48-661-351-015" data-aos="fade-up">+ 48 661 351 015</a><br>
                                <a href="tel:+48-790-495-310" data-aos="fade-up">+ 48 790 495 310</a><br>
                                <a href="tel:+64-226-27-0199" data-aos="fade-up">+ 64 226 27 0199</a></p>
                        </div>
                        <div class="contact-numbers"></div>
                        <?php echo do_shortcode('[wpgmza id="1"]');?>
                    </div>
                    <div class="contact-form">
                        <h2 data-aos="fade-up">Kontakt</h2>
                        <p data-aos="fade-up">Masz pytanie?<br> Napisz do nas, a my skontaktujemy się z Tobą!</p>
                        <?php echo do_shortcode('[contact-form-7 id="167" title="Form-pl"]');?>
                    </div>
                </div>
            </section>
        </main>

        <!-- #main -->

    </div>

    <!-- #primary -->



    <?php

            get_footer();

        ?>
