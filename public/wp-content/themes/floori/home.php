<?php
/**
 *  Template name: Home
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package floori
 */

get_header();
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <div class="header-blog" id="home">
                <div class="brand">
                    <img id="flori-logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/Frame.png" alt="floori logo" />
                    <span class="brand-bg"></span>
                </div>

                <div class="header-bg">
                </div>
                <!-- .header-bg END -->

            </div>

            <section id="posts-home-page" class="w-1200">

                <h1 class="text-dark text-c">Welcome to our Blog</h1>

                <container class="feat-container">
                    <?php
							//display_single_post(); //  wyswietli wszystkie posty jeden pod drugim z detalami
							display_all_published_posts()
						?>
                </container>
            </section>

        </main>
        <!-- #main END -->
    </div>
    <!-- .content-area END -->

    <?php
            get_footer();
        ?>
