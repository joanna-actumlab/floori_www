<?php
/*
* Template Name: Privacy Policy
* Template Post Type: post, page, product
*/
 
get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <div class="w-1200">

            <div class='single-post-content w-1200'>
                <p class='post-title'> Privacy Policy </p>
                <div class='post-content'>
                    This Privacy Policy ("Privacy Policy") explains how Floori ("Floori," "we" or "us") collect, use and share personally identifiable information of visitors of our web site (the "Site") and users of our products and services (the "Services"). Please note that the primary purpose of our Site and the Services it to allow you to upload and view in Augmented Reality flooring textures. When this Privacy Policy uses the generic term "information" it is intended to address the general use of information, and not your specific Site Information. By using the Site, Services, you consent to the collection, use and disclosure of your personally identifiable Site Information, as applicable, in accordance with this Privacy Policy.
                    <h3>1. Personally Identifiable Information We Collect and How We Use It </h3>
                    You can generally visit the Site without revealing any personally identifiable information about yourself. We do not collect personal information from our Site visitors without the Site visitor providing us with this information as set forth in this Privacy Policy. If you request to receive company or product information, or request information about specific Services, or provide comments about Services, you will be asked to provide contact information so that we can complete your request. We use this Site Information to fulfil your request. We may also use this Site Information to send you additional information about our Services on the Site that you may be interested in. Some of the Services require that you become a registered user and create a user account. This will require that you provide certain personally identifiable information, that may include (but not be limited to) your (and if you are enrolling as part of our Floori for Business program) your company's name, e-mail address, as well as telephone and address information.
                    <h3>2. Other Information We Collect and How We Use It</h3>
                    Floori may also automatically collect and analyse Site Information about your general usage of the Site, Services. We might track your usage patterns to see what features of the Site, Services you commonly use, Site traffic volume, frequency of visits, type and time of transactions, type of browser, browser language, IP address and operating system, and statistical information about how you use the Services. We only collect, track and analyse such Site Information in an aggregate manner that does not personally identify you. This aggregate data may be used to assist us in operating the Site and the Services provided to other third parties to enable them to better understand the operation of the Services, but such information will be in aggregate form only and it will not contain personally identifiable data.
                    <h3>3. Children</h3>
                    We recognize the privacy interests of children and we encourage parents and guardians to take an active role in their children's online activities and interests. This Site is not intended for children under the age of 13 and we endeavour not to collect any personally identifiable information from children under the age of 13. Floori targets its Services and this Site to adults and not to children under 13.
                    <h3>4. Disclosure of Information</h3>
                    We will share your personally identifiable Site Information with third parties only in the ways that are described in this privacy policy. These include, but are not limited to, sharing Site Information with service providers to allow them to fulfil your requests and to your employer if you are enrolled through the Floori for Business program as an employee. We do not sell your personal information to third parties. We may also disclose your personally identifiable information: (a) if we are required to do so by law, regulation or other government authority or otherwise in cooperation with an investigation of a governmental authority, (b) to enforce these Terms of Use, or (c) to protect the safety of Users of our Site and our Services. In addition, we may transfer your personally identifiable information to a successor entity upon a merger, consolidation or other corporate reorganization.
                    <h3>5. Data Retention</h3>
                    We retain Site Information and the personal data we process on behalf of Users for as long as needed to provide the Services. We will retain and use this personal Site Information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements with Users, and/or the period required by laws in applicable jurisdictions.
                    <h3>6. Updating, Correcting and Deleting Your Information; Opt-out</h3>
                    If you believe that Floori has incorrect Site Information that was provided as part of the Services, you may use the Services to correct, amend, or delete that information. You may also contact us by e-mailing our customer support at hello@ActumLab.com. We can only alter Information that was initially provided to us. From time to time, we may send you emails about new or modified Services that we believe might interest you. If you wish to opt-out of receiving these materials, you may follow the unsubscribe procedure provided in the email.
                    <h3>7. Security</h3>
                    The security of your personal information is important to us. Floori employs procedural and technological measures to protect your personally identifiable information. These measures are reasonably designed to help protect your personally identifiable information from loss, unauthorized access, disclosure, alteration or destruction. We may use software, secure socket layer technology (SSL) encryption, password protection, firewalls, internal restrictions and other security measures to help prevent unauthorized access to your personally identifiable information. However, Floori cannot guarantee that unauthorized third parties will never be able to defeat these measures or use your personally identifiable information for improper purposes. Therefore, you should be aware that when you voluntarily display or distribute personally identifiable information, that information could be collected and used by others. Floori is not responsible for the unauthorized use by third parties of information you post or otherwise make available publicly.
                    <h3>8. Links to Other Websites</h3>
                    The Site, Services may provide links to third party web sites or resources not associated with us and over which we do not have control ("External Web Sites"). Such links do not constitute an endorsement by Floori of the External Web Sites, the content displayed therein, or the persons or entities associated therewith. You acknowledge that Floori is only providing these links as a convenience, and cannot be responsible for the content of such External Web Sites.
                    <h3>9. Material Changes</h3>
                    Floori reserves the right to change this Privacy Policy from time to time. We will post the revised Privacy Policy on this Site. We encourage you to review this Privacy Policy regularly for any changes. Your continued use of the Site and/or Services will be subject to the then-current Privacy Policy.
                    <h3>10. Contact Us</h3>
                    We welcome your comments or questions concerning our Privacy Policy. If you would like to contact Floori regarding this Privacy Policy, please contact us by emailing us at Hello@ActumLab.com
                </div>
            </div>
        </div>

    </main>
    <!-- #main -->
</div>
<!-- #primary -->

<?php
		get_footer();
	?>
