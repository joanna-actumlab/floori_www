/* loading IFRAMES later so that the page loads quicker */
function init() {
    var vidDefer = document.getElementsByTagName('iframe');
    for (var i = 0; i < vidDefer.length; i++) {
        if (vidDefer[i].getAttribute('data-src')) {
            vidDefer[i].setAttribute('src', vidDefer[i].getAttribute('data-src'));
        }
    }
}
//window.onload = init;
///* END of IFRAME loading */
///* contact form validation available on both sites front-page and #pricing */
//jQuery(window).on('load', function () {
//    var termsConfirmed = true;
//    var emailConfirmed = false;
//    var messageNotEmpty = false;
//
//    jQuery('#email').focusout(function () {
//        var input = jQuery(this);
//        var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
//        var is_email = pattern.test(input.val());
//        if (is_email) {
//            emailConfirmed = true;
//        } else {
//            emailConfirmed = false;
//        }
//    });
//
//    jQuery('#message').focusout(function () {
//        var message = jQuery(this);
//        if (message.val().length > 0) {
//            messageNotEmpty = true;
//        } else {
//            messageNotEmpty = false;
//        }
//    });
//
//    jQuery('#checkbox').click(function () {
//        if (termsConfirmed) {
//            termsConfirmed = false;
//        } else {
//            termsConfirmed = true;
//        }
//    });
//
//    jQuery('#submit').click(function (e) {
//        e.preventDefault();
//        var name = jQuery('#name').val();
//        var email = jQuery('#email').val();
//        var company = jQuery('#company').val();
//        var message = jQuery('#message').val();
//        var userData = 'name=' + name + '&email=' + email + '&company=' + company + '&message=' + message;
//
//        if (termsConfirmed && emailConfirmed && messageNotEmpty) {
//            jQuery.ajax({
//                type: "POST",
//                url: './mail.php',
//                data: userData,
//                success: function () {
//                    alertify.success('E-mail has been sent.');
//                }
//            });
//        } else {
//            if (termsConfirmed == false && emailConfirmed == false) {
//                alertify.error('Wrong email. Terms not agreed');
//            } else if (termsConfirmed == false) {
//                alertify.error('Please accept Terms.');
//            } else if (emailConfirmed == false) {
//                alertify.error('Please check your email.');
//            } else if (messageNotEmpty == false) {
//                alertify.error('Message field empty.');
//            }
//        }
//        if (termsConfirmed && emailConfirmed && messageNotEmpty) {
//            this.form.reset();
//            termsConfirmed = true;
//        }
//    });
//});

function linkToMain() {
    var x = document.getElementById("flori-logo");
    window.location = "/";
};
jQuery("#flori-logo").on("click", linkToMain);

function linkToFlooring() {
    var x = document.getElementById("toggle-flooring");
    window.location = "/flooring";
};
jQuery("#toggle-flooring").on("click", linkToFlooring);


/* RODO popup */
jQuery(document).ready(function () {
    jQuery.fn.cookiesEU({
        text: 'Dear user! On May 25, 2018, Regulation (EU) 2016/679 of the European Parliament and of the Council of April 27, 2016 (GDPR) will become effective. Familiarize yourself with our privacy policy to find out how we process your personal information. <a href="https://floori.io/privacy-policy/" class="policy">Privacy Policy</a>, <a href="https://floori.io/terms-of-use/" class="policy">Terms of use</a>',
        close: 'Accept',
        position: 'bottom',
        box_css: 'background-color:rgba(0,0,0,0.8);bottom:0;height:auto;position:fixed;width:100%;z-index:10000;',
        text_css: 'color:white;padding:2em; max-width: 70vw; display:block; margin:auto; position:relative;top:1em;border-radius:5px;font-size:1.3em;line-height:1.3em;',
        close_css: 'background-color:#F15A24;position:relative;bottom:1em; max-width: 200px; text-align:center;padding:10px;display:block;margin:0 auto 10px auto;border-radius:5px;margin-top:10px;border:2px solid #fff;',
        auto_accept: false,
        test: false,
    });
});

jQuery(document).ready(function () {
    jQuery('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        var target = this.hash;
        $target = jQuery(target);
        if ($target.length) {
            jQuery('html, body').stop().animate({
                'scrollTop': $target.offset().top //no need of parseInt here
            }, 900, 'swing', function () {
                window.location.hash = target;
            });
        }
    });
});

/* slick gallery on front-page section: #our-clients */
jQuery("#our-clients").ready(function () {
    jQuery('.responsive').slick({
        autoplay: true,
        infinite: true,
        dots: true,
        speed: 300,
        autoplaySpeed: 1000,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                /* poniżej 1024  */
                settings: {
                    autoplay: true,
                    infinite: true,
                    dots: true,
                    slidesToShow: 3,
                    slidesToScroll: 3

                }
          },
            {
                breakpoint: 600,
                /* poniżej 600  */
                settings: {
                    autoplay: true,
                    slidesToShow: 2,
                    slidesToScroll: 2

                }
          },
            {
                breakpoint: 480,
                /* poniżej 480  */
                settings: {
                    autoplay: true,
                    fade: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
          }

        ]
    });
});


/* " #flooring " site JS  - flooring gallery */
jQuery(".flooring").ready(function () {
    var itemsHaveBeenFound = false;
    var adres = myScript.theme_directory + '/assets/img/floors/';
    const companiesArray = [
        /* Add new element in table and drop your image to:
         *  floori/assets/img/floors/YourFileName.png *
         *  company name must be in lowerletters ['companyName', adres + 'fileName', 'websiteURL']
         * styles in global section : 'FLOORING'
         */
        ['walczak', adres + 'walczak.png', "http://www.walczakparkiety.pl/"],
        ['tajima', adres + 'tajima.png', "https://www.tajima-europe.com/"],
        ['gerflor', adres + 'gerflor.png', "https://www.gerflor.com/product-ranges.html"],
        ['tarkett', adres + 'tarkett.png', "https://home.tarkett.com/en_EU/"],
        ['swiss krono', adres + 'swiss.png', "https://www.swisskrono.pl/en"],
        ['kaczkan', adres + 'kaczkan.png', "http://www.kaczkan.pl/en"],
        ['finish parkiet', adres + 'finish_parkiet.png', "https://www.finishparkiet.pl/en"],
        ['barlinek', adres + 'barlinek.png', "http://www.barlinek.com.pl/en"],
        ['classen', adres + 'classen.png', "https://www.classen.de/lang/en"],
        ['quick step', adres + 'quick_step.png', "https://www.quick-step.com/en"],
        ['desso', adres + 'desso.png', "http://www.desso.com/"],
        ['forbo', adres + 'forbo.png', "https://www.forbo.com/corporate/en-gl/"],
       //['faus', adres + 'faus.png', "https://www.faus.international/en/"],
        ['axpro carpets', adres + 'axpro.png', "http://www.axprocarpets.com/en"],
        ['wicanders', adres + 'wicanders.png', "https://usa.wicanders.com/en/"]
       //['burmatex', adres + 'burmatex.png', "https://www.burmatex.co.uk/#accept"],
       //['paragon', adres + 'paragon.png', "https://www.paragon-carpets.co.uk/"],
        //['heckmondwike', adres + 'heckmondwike.png', "https://heckmondwike-fb.co.uk/"],
        //['uk contract flooring', adres + 'uk_contract_flooring.png', "https://www.ukcontractflooring.co.uk/"],
        //['cavalier carpets', adres + 'cavalier.png', "http://www.cavaliercarpets.co.uk/"],
        //['lano', adres + 'lano.png', "https://www.lano.com/en"],
        //['moduleo',adres + 'moduleo.png', "https://www.moduleo.com/"],
        //['brockway',adres + 'brockway.png', "https://www.brockway.co.uk/"],
        //['cormar carpets', adres + 'cormar.png', "https://www.cormarcarpets.co.uk/"],
        //['penthouse carpets', adres + 'penthouse_carpets.png', "http://www.penthousecarpets.co.uk/"]
    ];
    jQuery(".flooring").ready(function () { // wyswietla wszystkie loga klientow (zanim zaczniemy pisac)
        atStartappendDivsWithaAllBackgroundLogosAvailable();

        jQuery('div.image').click(function () {
            var selectedElementId = this.id;
            jQuery(location).attr('href', companiesArray[selectedElementId][2]);
        });
    });

    function atStartappendDivsWithaAllBackgroundLogosAvailable() {
        var divArray = [];
        for (let i = 0; i < companiesArray.length; i++) {
            var link = companiesArray[i][1];
            var url = `url('${link}');`;
            var text = companiesArray[i][0];
            var alt = `${text}`;
            var background = `"background-image: ${url}"`;
            divArray.push("<div id=" + `"${i}"` + "class='image'style=" + `${background}` + "alt=" + `"${alt}"` + "></div> ");
        }
        jQuery(".logo-box").append(divArray); // Append the new elements 
    }

    function visualizeFoundCompaniesLogos(tablica) {
        hideAllBackgroundLogos();
        for (let i = 0; i < tablica.length; i++) {
            jQuery("#" + tablica[i]).css("display", "inline-block");
            jQuery(".notify").remove();
        }
    }

    function hideAllBackgroundLogos() {
        for (let i = 0; i < companiesArray.length; i++) {
            jQuery("#" + i).css("display", "none");
        }
    }

    function showAllBackgroundLogos() {
        jQuery(".notify").remove();
        for (let i = 0; i < companiesArray.length; i++) {
            jQuery("#" + i).css("display", "inline-block");
        }
    }
    jQuery('#flooring-company').keypress(function (e) {
        if (e.which == 13) {
            jQuery(this).focusout();
        }
    });
    jQuery('#flooring-company').focusout(function () {
        jQuery(".logo-box").removeClass("full");
        jQuery(".logo-box").addClass("empty");
        if (jQuery('#flooring-company').val() == '') {
            showAllBackgroundLogos();

        }
    });
    jQuery('#flooring-company').keyup(function () { // focusout
        var input = jQuery(this).val().toLowerCase();
        var tablica = [];
        if (input.length) {
            for (let i = 0; i < companiesArray.length; i++) {
                if (companiesArray[i][0].indexOf(input) != -1) {
                    tablica.push(i);
                } else {
                    jQuery("#" + i).css("display", "none");
                    itemsHaveBeenFound = true;
                }
            }
            visualizeFoundCompaniesLogos(tablica);
            if (tablica.length == 0) {
                itemsHaveBeenFound = false;
            }
        }
        if (jQuery(".logo-box").hasClass("empty") && itemsHaveBeenFound) {
            jQuery(".logo-box").removeClass("empty");
            jQuery(".logo-box").addClass("full");
        } else if (!itemsHaveBeenFound && jQuery('#flooring-company').val() != '') {
            if (!jQuery("h1").hasClass("not-found")) {
                jQuery(".logo-box").append(
                    "<h1 class='notify not-found'>" +
                    "Don't see your company up there? Drop us a line at hello@floori.io and we'll work with you to bring your inventory to the masses!" +
                    "<a class='contact-btn' href='/#contact'>Contact us!</a>" +
                    "</h1>"
                );
            }
        } else if (jQuery(".logo-box").hasClass("full") && jQuery('#flooring-company').val() == '') {
            showAllBackgroundLogos();
        }
    });
});
/* flooring logo boot end */
