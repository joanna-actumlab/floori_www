(function ($) {
    var elementIsClicked = false;
    if ($(window).width() < 1025) {
        $(".toggle").click(function () {

            $(".toggle").toggleClass("on");
            $(".topnav li").slideToggle();
        });

        $(".topnav a").click(function () {
            $(".toggle").removeClass("on");
            $(".topnav li").fadeOut(400);
        });
    }

})(jQuery);
