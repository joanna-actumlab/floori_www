//     $('a[href^="#"]').on('click', function (e) {
//     e.preventDefault();
//     var target = this.hash;
//     $target = $(target);
//     if ($target.length) {
//         $('html, body').stop().animate({
//             'scrollTop': $target.offset().top //no need of parseInt here
//         }, 900, 'swing', function () {
//             window.location.hash = target;
//         });
//     }
//     });
//     });


jQuery(document).ready(function ($) {
    $('.menu-item > a').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 40
                }, 900);
                return false;
            }
        }
    });
});
