<?php
/*
* Template Name: Flooring
* Template Post Type: post, page, product
*/
 
get_header();
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <div class="header-blog" id="home">
                <div class="brand">
                    <img id="flori-logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/Frame.png" alt="floori logo" />
                    <span class="brand-bg"></span>
                </div>
                <div class="header-bg">
                </div>
                <!-- .header-bg END -->
            </div>

            <section id="logo-search-page" class="flooring w-1200">
                <!-- poczatek ZMIAN -->

                <p class='post-title'> Search for flooring </p>
                <input type="text" name="flooring-company" id="flooring-company" placeholder="Type company name here.." required>

                <div class='logo-box-wrapper '>
                    <div class='logo-box empty '>

                    </div>
                </div>
            </section>

        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
		get_footer();
	?>
