<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package floori
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Description" content="Floori application app flooring structure sensor">
    <title>Floori - Powering the Flooring sales experience</title>
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="page" class="site">
        <header id="masthead" class="site-header">

            <nav id="site-navigation" class="main-navigation">
                <div class="w-1200 relative">
                    <div class="brand">
                        <img id="flori-logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/floori-logo.svg" alt="floori logo" />
                    </div>
                    <div id="myTopnav" class="topnav">
                        <?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
				?>

                    </div>
                    <div class="mobile-nav-toggle" id="mobile-nav-toggle">
                        <!-- onclick="menu-toggle()" -->
                        <div class="toggle">
                            <div class="one"></div>
                            <div class="two"></div>
                            <div class="three"></div>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- #site-navigation -->
        </header>
        <!-- #masthead -->

        <div id="content" class="site-content">
