<?php

/**

 * Template name: front-page

 *

 * This is the template that displays all pages by default.

 * Please note that this is the WordPress construct of pages

 * and that other 'pages' on your WordPress site may use a

 * different template.

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *

 * @package floori

 */



get_header();

?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <?php while ( have_posts() ) : the_post(); ?>
        <div class="header" id="home">
            <div class="header-wrapper">
                <div class="header-gif ipad-gif"></div>
                <div class="title">
                    <?php if( have_rows('header') ): ?>
                    <?php while ( have_rows('header') ) : the_row(); ?>
                    <h2>
                        <?php the_sub_field('header-title'); ?>
                    </h2>
                    <p>
                        <?php  the_sub_field('header-desc'); ?>
                    </p>

                    <?php echo do_shortcode('[cf7form cf7key="287"]')?>
                    <?php if( have_rows('button-header') ): ?>
                    <div class="header-buttons" data-aos="fade-up">

                        <?php while ( have_rows('button-header') ) : the_row(); ?>
                        <a class="button" href="<?php the_sub_field('button-header-link'); ?>">
                            <p>
                                <?php the_sub_field('button-header-txt'); ?>
                            </p>
                        </a>

                        <?php endwhile; ?>


                    </div>
                    <?php endif; ?>

                    <?php endwhile; ?>
                    <?php endif; ?>


                </div>
            </div>
            <div class="header-bg"></div>
        </div>



        <?php if( have_rows('features') ): ?>
        <section id="features" class="w-1200" data-aos="fade-up" data-aos-delay="200">
            <?php while ( have_rows('features') ) : the_row(); ?>
            <h2>
                <?php the_sub_field('header-tagline'); ?>
            </h2>

            <?php if( have_rows('feat-box') ): ?>
            <container class="feat-container">
                <?php while ( have_rows('feat-box') ) : the_row(); ?>
                <div class="feat-box">
                    <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php the_sub_field('feat-img'); ?>');">

                    </div>
                    <h3 class="text-c">
                        <?php the_field('feat-title'); ?>
                    </h3>
                    <p>
                        <?php the_sub_field('feat-desc'); ?>
                    </p>
                </div>
                <?php endwhile; ?>
            </container>
            <?php endif; ?>
            <?php endwhile; ?>
        </section>
        <?php endif; ?>

        <?php if( have_rows('section') ): ?>
        <section id="free-trial" data-aos="fade-up" data-aos-delay="200">
            <?php while ( have_rows('section') ) : the_row(); ?>
            <div class="w-1200">
                <h2 class="text-c" data-aos="fade-up" data-aos-delay="200">
                    <?php the_sub_field('section-title'); ?>
                </h2>
                <p class="text-c " data-aos="fade-up" data-aos-delay="200">
                    <?php the_sub_field('section-desc'); ?>
                </p>

                <?php echo do_shortcode('[cf7form cf7key="trial-form"]')?>
            </div>
            <?php endwhile; ?>
        </section>
        <?php endif; ?>

        <?php if( have_rows('options') ): ?>
        <section id="options" class="options" data-aos="fade-up">
            <?php while ( have_rows('options') ) : the_row(); ?>
            <h2 class="text-c">
                <?php the_sub_field('options-title'); ?>
            </h2>
            <?php if( have_rows('plan') ): ?>
            <div class="options-wrapper">
                <?php while ( have_rows('plan') ) : the_row(); ?>
                <div class="option">
                    <h2 class="text-c">
                        <?php the_sub_field('plan-title'); ?>
                    </h2>
                    <p class="plan-desc">
                        <?php the_sub_field('plan-desc'); ?>
                    </p>
                    <?php if( have_rows('plan-list') ): ?>
                    <ul>
                        <?php while ( have_rows('plan-list') ) : the_row(); ?>
                        <li data-aos="fade-left"><i></i>
                            <p>
                                <?php the_sub_field('plan-list-item'); ?>
                            </p>
                        </li>
                        <?php endwhile; ?>
                    </ul>
                    <?php endif; ?>
                    <?php if( have_rows('plan-button') ): ?>
                    <?php while ( have_rows('plan-button') ) : the_row(); ?>
                    <a class="button btn-orange m-auto" href="<?php the_sub_field('button-link'); ?>">
                        <p>
                            <?php the_sub_field('button-txt'); ?>
                        </p>
                    </a>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
            <?php endwhile; ?>
        </section>
        <?php endif; ?>


        <?php if( have_rows('clients') ): ?>
        <section id="our-clients" data-aos="fade-up">
            <?php while ( have_rows('clients') ) : the_row(); ?>
            <h1 class="text-dark text-c">
                <?php the_sub_field('clients-title'); ?>
            </h1>
            <div class="grid-wrapper">
                <?php 
$images = get_sub_field('clients-gallery');
if( $images ): ?>
                <?php foreach( $images as $image ): ?>
                <div class="customer-logo">
                    <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <?php endwhile; ?>
        </section>
        <?php endif; ?>

        <?php if( have_rows('youtube') ): ?>
        <section class="social-media">

            <?php while ( have_rows('youtube') ) : the_row(); ?>
            <h1 data-aos="fade-up">
                <?php the_sub_field('youtube-title'); ?>
            </h1>

            <?php if( have_rows('videos') ): ?>

            <div class="w-1200 youtube-wrapper" data-aos="fade-up">
                <?php while ( have_rows('videos') ) : the_row(); ?>
                <div class="video-frame" data-aos="fade-up">
                    <?php the_sub_field( 'embed' ); ?>
                </div>
                <?php endwhile; ?>
            </div>

            <?php endif; ?>
            <?php endwhile; ?>
        </section>
        <?php endif; ?>

        <section id="contact" class="contact">
            <div class="contact-wrapper">
                <?php if( have_rows('find-us') ): ?>
                <div class="contact-info">
                    <?php while ( have_rows('find-us') ) : the_row(); ?>
                    <h2 data-aos="fade-up">
                        <?php the_sub_field('find-us-title');?>
                    </h2>
                    <?php if( have_rows('numbers') ): ?>
                    <div class="contact-numbers">
                        <?php while ( have_rows('numbers') ) : the_row(); ?>
                        <p style="margin-bottom:0px;"><span data-aos="fade-up">
                                <?php the_sub_field('city-item');?></span></p>
                        <p style="margin-bottom:0px; text-align: right;"><a href="tel:+1-616-485-6925" data-aos="fade-up">
                                <?php the_sub_field('number-item');?></a></p>
                        <?php endwhile;?>
                    </div>
                    <?php endif;?>
                    <div class="contact-numbers"></div>
                    <?php echo do_shortcode('[wpgmza id="1"]');?>
                    <?php endwhile;?>
                </div>
                <?php endif;?>
                <?php if( have_rows('contact-us') ): ?>
                <div class="contact-form">

                    <?php while ( have_rows('contact-us') ) : the_row(); ?>
                    <h2 data-aos="fade-up">
                        <?php the_sub_field('contact-title');?>
                    </h2>
                    <p data-aos="fade-up">
                        <?php the_sub_field('contact-desc');?>
                    </p>
                    <?php echo do_shortcode('[cf7form cf7key="form-main-2"]');?>
                    <?php endwhile;?>

                </div>
                <?php endif;?>
            </div>
        </section>
        <?php endwhile; // end of the loop. ?>
    </main>

    <!-- #main -->

</div>

<!-- #primary -->



<?php
if (get_locale() == 'pl_PL') {

            get_footer('pl');}
else{
    get_footer();
}

        ?>
