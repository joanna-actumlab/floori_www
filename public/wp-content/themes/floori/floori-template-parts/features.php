<?php
/**
 * Template part for displaying contact
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package floori
 */

?>
<?php
    $currentlang = get_bloginfo('language');
    if($currentlang=="en-GB"):
?>
<?php if( have_rows('features') ): ?>
<section id="features" class="w-1200" data-aos="fade-up" data-aos-delay="200">
    <?php while ( have_rows('features') ) : the_row(); ?>
    <h2>
        <?php the_sub_field('header-tagline'); ?>
    </h2>

    <?php if( have_rows('feat-box') ): ?>
    <container class="feat-container">
        <?php while ( have_rows('feat-box') ) : the_row(); ?>
        <div class="feat-box">
            <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php the_sub_field('feat-img'); ?>');">

            </div>
            <h3 class="text-c">
                <?php the_field('feat-title'); ?>
            </h3>
            <p>
                <?php the_sub_field('feat-desc'); ?>
            </p>
        </div>
        <?php endwhile; ?>
    </container>
    <?php endif; ?>
    <?php endwhile; ?>
</section>
<?php endif; ?>
<?php elseif($currentlang=="pl-PL"): ?>
<?php if( have_rows('features') ): ?>
<section id="features" class="w-1200" data-aos="fade-up" data-aos-delay="200">
    <?php while ( have_rows('features') ) : the_row(); ?>
    <h2>
        <?php the_sub_field('header-tagline'); ?>
    </h2>

    <?php if( have_rows('feat-box') ): ?>
    <container class="feat-container">
        <?php while ( have_rows('feat-box') ) : the_row(); ?>
        <div class="feat-box">
            <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php the_sub_field('feat-img'); ?>');">

            </div>
            <h3 class="text-c">
                <?php the_field('feat-title'); ?>
            </h3>
            <p>
                <?php the_sub_field('feat-desc'); ?>
            </p>
        </div>
        <?php endwhile; ?>
    </container>
    <?php endif; ?>
    <?php endwhile; ?>
</section>
<?php endif; ?>
<?php endif; ?>
