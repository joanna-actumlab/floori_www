<?php
/**
  * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package floori
 */

?>

</div>
<!-- #content -->

<footer id="colophon" class="site-footer">

    <div id="footer">

        <div class="copyright">Made with
            <i class=" fa fa-heart" style="color:#f15a24;"></i> by
            <a href="https://actumlab.com/en/home/"> Actum Lab</a>
        </div>

        <div class="menu-footer">
            <ul>

                <li>
                    <a href="/terms-of-use">
                        Warukni korzystania ze strony
                    </a>
                </li>
                <li>
                    <a href="/privacy-policy">
                        Polityka prywatności
                    </a>
                </li>
                <li>
                    <a href="">
                        Prasa
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/flooriAR/?ref=br_rs" aria-label="facebook icon as link">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/floori_ar/" aria-label="instagram icon as link">
                        <i class="fab fa-instagram"></i>
                    </a>
                </li>
            </ul>
        </div>

        <div class="apple-copyright">
            <a>Apple, the Apple logo, and Apple iPad are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.</a>
        </div>

    </div>

    <a href="#home">
        <div class="arrow-up">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-up.svg" alt="arrow-up.svg">
        </div>
    </a>

</footer>
<!-- #colophon -->
</div>
<!-- #page -->

<?php wp_footer(); ?>


<script>
    AOS.init();

</script>
</body>

</html>
