<?php
/**
* Template Name: Floori lite pl
* Template Post Type: post, page, product
 *

 * @package floori
 */

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <div class="header" id="home">
            <div class="header-wrapper floori-lite">
                <div class="header-gif">
                </div>
                <div class="title">
                    <h1>Floori lite</h1>
                    <p>to prosty ale potężny wizualizator podłóg na Twoja stronę internetowa lub sklep e-commerce, w którym możesz użyć własnych zdjęć
                    </p>

                    <div class="header-buttons" data-aos="fade-up">

                        <a class="button" href="/floori-lite/#how-it-works">
                            <p>Jak to działa?</p>
                        </a>
                        <a class="button button-animation" href="/floori-lite-demo">
                            <p>Wypróbuj!</p>
                        </a>
                    </div>

                </div>
            </div>
            <div class="header-bg"></div>
        </div>

        <section id="features" class="w-1200" data-aos="fade-up" data-aos-delay="200">
            <!--
<h2 class="text-c">Floori - the ultimate flooring sales tool: helping you close more deals,
    <span style="color: #f15a24;">faster</span>
</h2>
-->
            <container class="feat-container">
                <div class="feat-box">
                    <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/clock.svg');">

                    </div>
                    <h3 class="text-c">Bez instalacji aplikacji mobilnej
                    </h3>
                    <p>Floori Lite to narzędzie web’owe, które nie wymaga instalowania aplikacji mobilnej
                    </p>
                </div>
                <div class="feat-box">
                    <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/ai.png');">

                    </div>
                    <h3 class="text-c">Rekomendacje AI</h3>
                    <p>
                        Floori używa zaawansowanych algorytmów Sztucznej Inteligencji (AI) do analizy kolorystyki i obiektów w pomieszczeniu, aby zarekomendować najlepszą podłogę.
                    </p>
                </div>
                <div class="feat-box">
                    <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/trending-up.svg');">

                    </div>
                    <h3 class="text-c">Potężne narzędzie sprzedażowe</h3>
                    <p>Floori znacząco zwiększa sprzedaż oraz konwersję, ułatwiając klientom podjęcie decyzji</p>
                </div>
            </container>
        </section>

        <section id="how-it-works">

            <h1 class="text-dark text-c">How it works?</h1>
            <div class="how-wrapper">

                <div class="graph" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/floori-lite-graph.svg');"> </div>
                <div class="lite-gif"></div>
                <!--       <p class="text-under-movie   text-dark">Floori tekst</p>-->
            </div>
        </section>
        <section id="our-clients" data-aos="fade-up">
            <h1 class="text-dark text-c">We're powering their sales experience!</h1>
            <div class="grid-wrapper w-1200">
                <!--
                <?php
                            display_available_logos("customers");
                        ?>
-->
                <div class="customer-logo" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/customers/grayscale/logo_1.png');"></div>

                <div class="customer-logo" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/customers/grayscale/logo_2.png');"></div>

                <div class="customer-logo" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/customers/grayscale/logo_3.png');"></div>

                <div class="customer-logo" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/customers/grayscale/logo_4.png');"></div>

                <div class="customer-logo" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/customers/grayscale/logo_5.png');"></div>

                <div class="customer-logo" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/customers/grayscale/logo_6.png');"></div>


            </div>
        </section>
        <section id="contact" class="contact">
            <div class="contact-wrapper">
                <div class="contact-info">
                    <h2 data-aos="fade-up">Where to find us?</h2>
                    <div class="contact-numbers">
                        <p><span data-aos="fade-up">Austin, USA</span><br> <span data-aos="fade-up">Warsaw, Poland</span><br> <span data-aos="fade-up">Gdansk, Poland</span><br> <span data-aos="fade-up">Dublin, Ireland</span><br> <span data-aos="fade-up">Wellington, New Zealand</span></p>
                        <p><a href="tel:+1-616-485-6925" data-aos="fade-up">+ 1 616 485 6925</a><br>
                            <a href="tel:+48-790-495-310" data-aos="fade-up">+ 48 790 495 310</a><br>
                            <a href="tel:+48-661-351-015" data-aos="fade-up">+ 48 661 351 015</a><br>
                            <a href="tel:+48-790-495-310" data-aos="fade-up">+ 48 790 495 310</a><br>
                            <a href="tel:+64-226-27-0199" data-aos="fade-up">+ 64 226 27 0199</a></p>
                    </div>
                    <div class="contact-numbers"></div>
                    <?php echo do_shortcode('[wpgmza id="1"]');?>
                </div>
                <div class="contact-form">
                    <h2 data-aos="fade-up">Contact Us</h2>
                    <p data-aos="fade-up">Questions, ideas or feedback?<br> Send us a message and we’ll get back to you as soon as possible!</p>
                    <?php echo do_shortcode('[contact-form-7 id="145" title="Form-eng"]');?>
                </div>
            </div>
        </section>
    </main>
    <!-- #main -->
</div>
<!-- #primary -->

<?php
get_footer();
