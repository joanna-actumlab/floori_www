<?php
/**
* Template Name: Floori lite
* Template Post Type: post, page, product
 *

 * @package floori
 */

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <?php while ( have_posts() ) : the_post(); ?>
        <div class="header" id="home">
            <div class="header-wrapper floori-lite">
                <div class="header-gif">
                </div>
                <div class="title">
                    <?php if( have_rows('header_lite') ): ?>
                    <?php while ( have_rows('header_lite') ) : the_row(); ?>
                    <h2>
                        <?php the_sub_field('header-title'); ?>
                    </h2>
                    <p>
                        <?php  the_sub_field('header-desc'); ?>
                    </p>

                    <?php echo do_shortcode('[cf7form cf7key="287"]')?>
                    <?php if( have_rows('button-header') ): ?>
                    <div class="header-buttons" data-aos="fade-up">

                        <?php while ( have_rows('button-header') ) : the_row(); ?>
                        <a class="button" href="<?php the_sub_field('button-header-link'); ?>">
                            <p>
                                <?php the_sub_field('button-header-txt'); ?>
                            </p>
                        </a>

                        <?php endwhile; ?>


                    </div>
                    <?php endif; ?>

                    <?php endwhile; ?>
                    <?php endif; ?>


                </div>
            </div>
            <div class="header-bg"></div>
        </div>

        <?php if( have_rows('features_lite') ): ?>
        <section id="features" class="w-1200" data-aos="fade-up" data-aos-delay="200">
            <?php while ( have_rows('features_lite') ) : the_row(); ?>
            <h2>
                <?php the_sub_field('header-tagline'); ?>
            </h2>

            <?php if( have_rows('feat-box') ): ?>
            <container class="feat-container">
                <?php while ( have_rows('feat-box') ) : the_row(); ?>
                <div class="feat-box">
                    <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php the_sub_field('feat-img'); ?>');">

                    </div>
                    <h3 class="text-c">
                        <?php the_field('feat-title'); ?>
                    </h3>
                    <p>
                        <?php the_sub_field('feat-desc'); ?>
                    </p>
                </div>
                <?php endwhile; ?>
            </container>
            <?php endif; ?>
            <?php endwhile; ?>
        </section>
        <?php endif; ?>

        <?php if( have_rows('technology') ): ?>
        <section id="technology" class="technology">
            <?php while ( have_rows('technology') ) : the_row(); ?>
            <h1 class="text-c">
                <?php the_sub_field('tech-title'); ?>
            </h1>

            <div class="technology-wrapper w-1200">

                <div class="ipad" data-aos="fade-up" style="background-image: url('<?php the_sub_field('tech-img'); ?>');"></div>
                <div class="tech-list">

                    <ul>
                        <?php while ( have_rows('tech-list') ) : the_row(); ?>
                        <a href="https://www.apple.com/ipad/">
                            <li data-aos="fade-left"><i></i>
                                <p>
                                    <?php the_sub_field('tech-list-item'); ?>
                                </p>
                            </li>
                        </a>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>


            <?php endwhile; ?>


        </section>
        <?php endif; ?>



        <?php
if (is_page( 'floori-lite-pl' ) ):?>
        <?php if( have_rows('clients',261) ): ?>
        <section id="our-clients" data-aos="fade-up">
            <?php while ( have_rows('clients',261) ) : the_row(); ?>
            <h1 class="text-dark text-c">
                <?php the_sub_field('clients-title'); ?>
            </h1>
            <div class="grid-wrapper">
                <?php 
$images = get_sub_field('clients-gallery');
if( $images ): ?>
                <?php foreach( $images as $image ): ?>
                <div class="customer-logo">
                    <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <?php endwhile; ?>
        </section>
        <?php endif; ?>

        <?php endif;
?>
        <?php
if (is_page( 'floori-lite' ) ):?>
        <?php if( have_rows('clients',6) ): ?>
        <section id="our-clients" data-aos="fade-up">
            <?php while ( have_rows('clients',6) ) : the_row(); ?>
            <h1 class="text-dark text-c">
                <?php the_sub_field('clients-title'); ?>
            </h1>
            <div class="grid-wrapper">
                <?php 
$images = get_sub_field('clients-gallery');
if( $images ): ?>
                <?php foreach( $images as $image ): ?>
                <div class="customer-logo">
                    <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <?php endwhile; ?>
        </section>
        <?php endif; ?>

        <?php endif;
?>
        <?php
if (is_page( 'floori-lite-pl' ) ):?>
        <section id="contact" class="contact">
            <div class="contact-wrapper">
                <?php if( have_rows('find-us', 261) ): ?>
                <div class="contact-info">
                    <?php while ( have_rows('find-us', 261) ) : the_row(); ?>
                    <h2 data-aos="fade-up">
                        <?php the_sub_field('find-us-title');?>
                    </h2>
                    <?php if( have_rows('numbers') ): ?>
                    <div class="contact-numbers">
                        <?php while ( have_rows('numbers') ) : the_row(); ?>
                        <p style="margin-bottom:0px;"><span data-aos="fade-up">
                                <?php the_sub_field('city-item');?></span></p>
                        <p style="margin-bottom:0px; text-align: right;"><a href="tel:+1-616-485-6925" data-aos="fade-up">
                                <?php the_sub_field('number-item');?></a></p>
                        <?php endwhile;?>
                    </div>
                    <?php endif;?>
                    <div class="contact-numbers"></div>
                    <?php echo do_shortcode('[wpgmza id="1"]');?>
                    <?php endwhile;?>
                </div>
                <?php endif;?>
                <?php if( have_rows('contact-us',261) ): ?>
                <div class="contact-form">

                    <?php while ( have_rows('contact-us',261) ) : the_row(); ?>
                    <h2 data-aos="fade-up">
                        <?php the_sub_field('contact-title');?>
                    </h2>
                    <p data-aos="fade-up">
                        <?php the_sub_field('contact-desc');?>
                    </p>
                    <?php echo do_shortcode('[cf7form cf7key="form-main-2"]');?>
                    <?php endwhile;?>

                </div>
                <?php endif;?>
            </div>
        </section>
        <?php endif;
?>

        <?php
if (is_page( 'floori-lite' ) ):?>
        <section id="contact" class="contact">
            <div class="contact-wrapper">
                <?php if( have_rows('find-us',6) ): ?>
                <div class="contact-info">
                    <?php while ( have_rows('find-us',6) ) : the_row(); ?>
                    <h2 data-aos="fade-up">
                        <?php the_sub_field('find-us-title');?>
                    </h2>
                    <?php if( have_rows('numbers') ): ?>
                    <div class="contact-numbers">
                        <?php while ( have_rows('numbers') ) : the_row(); ?>
                        <p style="margin-bottom:0px;"><span data-aos="fade-up">
                                <?php the_sub_field('city-item');?></span></p>
                        <p style="margin-bottom:0px; text-align: right;"><a href="tel:+1-616-485-6925" data-aos="fade-up">
                                <?php the_sub_field('number-item');?></a></p>
                        <?php endwhile;?>
                    </div>
                    <?php endif;?>
                    <div class="contact-numbers"></div>
                    <?php echo do_shortcode('[wpgmza id="1"]');?>
                    <?php endwhile;?>
                </div>
                <?php endif;?>
                <?php if( have_rows('contact-us',6) ): ?>
                <div class="contact-form">

                    <?php while ( have_rows('contact-us',6) ) : the_row(); ?>
                    <h2 data-aos="fade-up">
                        <?php the_sub_field('contact-title');?>
                    </h2>
                    <p data-aos="fade-up">
                        <?php the_sub_field('contact-desc');?>
                    </p>
                    <?php echo do_shortcode('[cf7form cf7key="form-main-2"]');?>
                    <?php endwhile;?>

                </div>
                <?php endif;?>
            </div>
        </section>
        <?php endif;
?>

        <?php endwhile; // end of the loop. ?>
    </main>
    <!-- #main -->
</div>
<!-- #primary -->



<?php
if (get_locale() == 'pl_PL') {

            get_footer('pl');}
else{
    get_footer();
}

        ?>
