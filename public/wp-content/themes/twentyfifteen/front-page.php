<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <section id="features" class="w-1200" data-aos="fade-up" data-aos-delay="200">
            <h2 class="text-c">Floori - the ultimate flooring sales tool: helping you close more deals,
                <span style="color: #f15a24;">faster</span>
            </h2>
            <?php echo do_shortcode( '[acf field="features"]' ) ?>
            <container class="feat-container">

                <div class="feat-box">
                    <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php echo get_field('featured-img'); ?>');">

                    </div>
                    <h3 class="text-c">Real-time view</h3>
                    <p>Real time rendering of your products– no marker or space selection needed!</p>
                </div>
                <div class="feat-box">
                    <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php echo get_field('featured-img'); ?>');">

                    </div>
                    <h3 class="text-c">Complete product catalog</h3>
                    <p>
                        Your entire inventory in the palm of your hand - no more expensive paper renders and catalogues!
                    </p>
                </div>
                <div class="feat-box">
                    <div class="feat-icon" data-aos="fade-up" style="background-image: url('<?php echo get_field('featured-img'); ?>');">

                    </div>
                    <h3 class="text-c">A powerful sales tool</h3>
                    <p>Floori increases sales volume significantly by making it easier for your customers to make decisions</p>
                </div>
            </container>
        </section>
    </div>
    <!-- .content-area -->

    <?php get_footer(); ?>
