<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oJGYkfGX6ztV53c0hFntzRNRFjQfv8VDcw9JoCZfh9bRyTIgiFjqwzSKOgMLkBzo/k/QuLXG9GBsD4pulZfSqg==');
define('SECURE_AUTH_KEY',  'xGoLbgWorBzvBAp1hcMcqZB7fmthHnFQwG3kZD3n+4mN7wBMJ7Qr/fTvZbcIDvq8NLqrKCDOz/TGR5PmYk2btQ==');
define('LOGGED_IN_KEY',    'c5uO+HRGndAGQtaQO1wU8TsaZrdiJJMfTfGeIpLAskPIhLtYqewbvTWZYwK5hQPZdmi4cS0xMv4gTUa/Ii0lOA==');
define('NONCE_KEY',        'cyEyHolHLMz1ysFXhWNca5QkMh0ddmCVqy93lXIjQvgJyZsOLSudfEmMEY6NXYBYuNQu8PDUCAvGX7QBOK5/vQ==');
define('AUTH_SALT',        'HPheAcRP46XHEQHlpoS2qEXO9P87gj1sDaoHQLj0pmZ4R3zUV240TLoT8xN/i/jmHqhKX29N4VVskoYPN7rmyQ==');
define('SECURE_AUTH_SALT', 'aVTOrRQzYufk5Gs4xm02zIiWhb+GlpeeETHckYy8IbR5xngfz4fW10hsAIDnoa0NAxmgg5ImNmemzceZR3hzJA==');
define('LOGGED_IN_SALT',   'wkq/6Eb8kcubirW12DkY3SesAPwcgoGY2gdURYXEgNTyuHxmF9bCmlYQgQftcANzoXi+qYEyJjF4av39lyFB+Q==');
define('NONCE_SALT',       'MkAypYW5r1K1iq+iyMrSIM2CNIYaL3UtJ/0SSzw9ENDqqCUsoFvIAh9Amnlv124Klsi4lzzuN1FjuCsNwfQR9w==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
